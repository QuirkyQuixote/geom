Simple Safe 2D/3D Types
=======================

A simple, header-only library to manipulate two- and three-dimensional spaces,
originally written as part of "Princess and Undead", for C++20.

Most types in this library have a tag that defines what the type is, and use
std::ratio<> to determine their actual magnitude in relation to other types
with the same tag.

Requires
--------

- C++ STL, of course,

Provides
--------

See [the documentation](doc/geom.md) for an explanation of the API.

Install
-------

Not strictly necessary since this is a header-only library, but `make install`
will install the headers in `$(PREFIX)$(includedir)/geom`
(by default `$(HOME)/.local/include/geom`).

You can remove the installed files with `make uninstall`.

Usage
-----

Include the required header files, use the [contained classes](doc/geom.md).

