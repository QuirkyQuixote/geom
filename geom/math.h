// Math - implement Vec, Interval, Box

// Copyright (C) 2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Geom.

// Geom is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Geom is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Geom.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GEOM_MATH_H_
#define GEOM_MATH_H_

#include <ranges>

#include "geom/num.h"

namespace geom {

template<number T, size_t N> struct Vec : public std::ranges::view_interface<Vec<T, N>> { };

template<number T, size_t N>
constexpr auto begin(Vec<T, N>& t) { return reinterpret_cast<T*>(&t); }
template<number T, size_t N>
constexpr auto end(Vec<T, N>& t) { return begin(t) + N; }
template<number T, size_t N>
constexpr auto begin(const Vec<T, N>& t) { return reinterpret_cast<const T*>(&t); }
template<number T, size_t N>
constexpr auto end(const Vec<T, N>& t) { return begin(t) + N; }

template<number T> struct Vec<T, 2> : public std::ranges::view_interface<Vec<T, 2>> {
        T x{}, y{};

        template<number X, number Y> constexpr Vec(X x, Y y) : x(x), y(y) {}
        template<number U> constexpr Vec(const Vec<U, 2>& r) : x(r.x), y(r.y) {}
        constexpr Vec() = default;

        template<number U> constexpr Vec& operator=(const Vec<U, 2>& r)
        { x = r.x; y = r.y; return *this; }
};

template<number T> struct Vec<T, 3> : public std::ranges::view_interface<Vec<T, 3>> {
        T x{}, y{}, z{};

        template<number X, number Y, number Z>
        constexpr Vec(X x, Y y, Z z) : x(x), y(y), z(z) {}
        template<number U> constexpr Vec(const Vec<U, 3>& r) : x(r.x), y(r.y), z(r.z) {}
        constexpr Vec() = default;

        template<number U> constexpr Vec& operator=(const Vec<U, 3>& r)
        { x = r.x; y = r.y; z = r.z; return *this; }
};

struct make_vec_fn {
        template<class... Args>
        constexpr auto operator()(Args&&... args) const
        { return Vec<std::common_type_t<std::decay_t<Args>...>,
                sizeof...(Args)>(std::forward<Args>(args)...); }
};

inline constexpr make_vec_fn make_vec;

template<number T> struct Interval;

struct make_interval_fn {
        template<number T, number U>
        constexpr auto operator()(T&& t, U&& u) const
        { return Interval<std::common_type_t<std::decay_t<T>,
                std::decay_t<U>>>(std::forward<T>(t), std::forward<U>(u)); }
};

inline constexpr make_interval_fn make_interval;

template<number T> struct Interval {
        T min{}, max{};

        template<number U> constexpr Interval(U min, U max) : min(min), max(max) {}
        constexpr Interval() = default;

        constexpr Interval& operator++()
        { ++min; ++max; return *this; }
        constexpr Interval& operator--()
        { --min; --max; return *this; }
        constexpr Interval operator++(int)
        { Interval r = *this; ++*this; return r; }
        constexpr Interval operator--(int)
        { Interval r = *this; --*this; return r; }

        constexpr auto operator+() const
        { return make_interval(+min, +max); }
        constexpr auto operator-() const
        { return make_interval(-max, -min); }
        constexpr auto operator~() const
        { return make_interval(~min, ~max); }
        constexpr auto operator!() const
        { return make_interval(!min, !max); }

        template<class U> constexpr auto operator+=(const U& r)
        { min += r; max += r; return *this; }
        template<class U> constexpr auto operator-=(const U& r)
        { min -= r; max -= r; return *this; }
        template<class U> constexpr auto operator*=(const U& r)
        { min *= r; max *= r; return *this; }
        template<class U> constexpr auto operator/=(const U& r)
        { min /= r; max /= r; return *this; }
        template<class U> constexpr auto operator%=(const U& r)
        { min %= r; max %= r; return *this; }
        template<class U> constexpr auto operator|=(const U& r)
        { min |= r; max |= r; return *this; }
        template<class U> constexpr auto operator&=(const U& r)
        { min &= r; max &= r; return *this; }
        template<class U> constexpr auto operator^=(const U& r)
        { min ^= r; max ^= r; return *this; }
        template<class U> constexpr auto operator<<=(const U& r)
        { min <<= r; max <<= r; return *this; }
        template<class U> constexpr auto operator>>=(const U& r)
        { min >>= r; max >>= r; return *this; }

        template<class U> constexpr auto operator+(const U& r) const
        { return make_interval(min + r, max + r); }
        template<class U> constexpr auto operator-(const U& r) const
        { return make_interval(min - r, max - r); }
        template<class U> constexpr auto operator*(const U& r) const
        { return make_interval(min * r, max * r); }
        template<class U> constexpr auto operator/(const U& r) const
        { return make_interval(min / r, max / r); }
        template<class U> constexpr auto operator%(const U& r) const
        { return make_interval(min % r, max % r); }
        template<class U> constexpr auto operator|(const U& r) const
        { return make_interval(min | r, max | r); }
        template<class U> constexpr auto operator&(const U& r) const
        { return make_interval(min & r, max & r); }
        template<class U> constexpr auto operator^(const U& r) const
        { return make_interval(min ^ r, max ^ r); }
        template<class U> constexpr auto operator<<(const U& r) const
        { return make_interval(min << r, max << r); }
        template<class U> constexpr auto operator>>(const U& r) const
        { return make_interval(min >> r, max >> r); }

        template<class U> constexpr bool operator==(const Interval<U>& r) const
        { return min == r.min && max == r.max; }
        template<class U> constexpr bool operator!=(const Interval<U>& r) const
        { return min != r.min || max != r.max; }
};

template<number T, size_t N> struct Box : public std::ranges::view_interface<Box<T, N>> {};

template<number T, size_t N>
constexpr auto begin(Box<T, N>& t) { return reinterpret_cast<Interval<T>*>(&t); }
template<number T, size_t N>
constexpr auto end(Box<T, N>& t) { return begin(t) + N; }
template<number T, size_t N>
constexpr auto begin(const Box<T, N>& t) { return reinterpret_cast<const Interval<T>*>(&t); }
template<number T, size_t N>
constexpr auto end(const Box<T, N>& t) { return begin(t) + N; }

template<number T> struct Box<T, 2> : public std::ranges::view_interface<Box<T, 2>> {
        T l{}, r{}, d{}, u{};

        template<number L, number R, number D, number U>
        constexpr Box(L l, R r, D d, U u) : l(l), r(r), d(d), u(u) {}
        template<number X, number Y>
        constexpr Box(Interval<X>&& x, Interval<Y>&& y)
                : l(x.min), r(x.max), d(y.min), u(y.max) {}
        template<number Min, number Max>
        constexpr Box(const Vec<Min, 2>& min, const Vec<Max, 2>& max)
                : l(min.x), r(max.x), d(min.y), u(max.y) {}
        template<number U> constexpr Box(const Box<U, 2>& _)
                : l(_.l), r(_.r), d(_.d), u(_.u) {}
        constexpr Box() = default;

        template<number U> constexpr Box& operator=(const Box<U, 2>& _)
        { l = _.l; r = _.r; d = _.d; u = _.u; return *this; }

        constexpr auto min() const { return make_vec(l, d); }
        constexpr auto max() const { return make_vec(r, u); }
};

template<number T> struct Box<T, 3> : public std::ranges::view_interface<Box<T, 3>> {
        T l{}, r{}, d{}, u{}, f{}, b{};

        template<number L, number R, number D, number U, number F, number B>
        constexpr Box(L l, R r, D d, U u, F f, B b)
                : l(l), r(r), d(d), u(u), f(f), b(b) {}
        template<number X, number Y, number Z>
        constexpr Box(Interval<X>&& x, Interval<Y>&& y, Interval<Z>&& z)
                : l(x.min), r(x.max), d(y.min), u(y.max), f(z.min), b(z.max) {}
        template<number Min, number Max>
        constexpr Box(const Vec<Min, 3>& min, const Vec<Max, 3>& max)
                : l(min.x), r(max.x), d(min.y), u(max.y), f(min.z), b(max.z) {}
        template<number U> constexpr Box(const Box<U, 3>& _)
                : l(_.l), r(_.r), d(_.d), u(_.u), f(_.f), b(_.b) {}
        constexpr Box() = default;

        template<number U> constexpr Box& operator=(const Box<U, 3>& _)
        { l = _.l; r = _.r; d = _.d; u = _.u; f = _.f; b = _.b; return *this; }

        constexpr auto min() const { return make_vec(l, d, f); }
        constexpr auto max() const { return make_vec(r, u, b); }
};

struct make_box_fn {
        template<class... Args>
        constexpr auto operator()(Interval<Args>&&... args) const
        { return Box<std::common_type_t<Args...>,
                sizeof...(Args)>(std::forward<Interval<Args>>(args)...); }

        template<number... Args>
        constexpr auto operator()(Args&&... args) const
        { return Box<std::common_type_t<Args...>,
                sizeof...(Args) / 2>(std::forward<Args>(args)...); }

        template<number T, size_t N>
        constexpr auto operator()(const Vec<T, N>& min, const Vec<T, N>& max) const
        { return Box<T, N>{min, max}; }
};

inline constexpr make_box_fn make_box;

template<class T> struct is_vector : std::integral_constant<bool, false> {};
template<number T, size_t N> struct is_vector<Vec<T, N>> : std::integral_constant<bool, true> {};
template<class T> inline constexpr bool is_vector_v = is_vector<T>::value;
template<class T> concept vector = is_vector_v<T>;

template<class T> struct is_interval : std::integral_constant<bool, false> {};
template<number T> struct is_interval<Interval<T>> : std::integral_constant<bool, true> {};
template<class T> inline constexpr bool is_interval_v = is_interval<T>::value;
template<class T> concept interval = is_interval_v<T>;

template<class T> struct is_box : std::integral_constant<bool, false> {};
template<number T, size_t N> struct is_box<Box<T, N>> : std::integral_constant<bool, true> {};
template<class T> inline constexpr bool is_box_v = is_box<T>::value;
template<class T> concept box = is_box_v<T>;

template<class T> struct geom_size : std::integral_constant<size_t, 0> {};
template<class T> static constexpr auto geom_size_v = geom_size<T>::value;
template<class T, size_t N> struct geom_size<Vec<T, N>> : std::integral_constant<size_t, N>{};
template<class T, size_t N> struct geom_size<Box<T, N>> : std::integral_constant<size_t, N>{};

template<class T> struct geom_value {};
template<class T> using geom_value_t = typename geom_value<T>::type;
template<class T, size_t N> struct geom_value<Vec<T, N>> { using type = T; };
template<class T, size_t N> struct geom_value<Box<T, N>> { using type = T; };

template<number T, size_t N> struct num_traits<Vec<T, N>> {
        using rep = typename num_traits<T>::rep;
        using units = typename num_traits<T>::units;
        using factor = typename num_traits<T>::factor;
};

template<number T> struct num_traits<Interval<T>> {
        using rep = typename num_traits<T>::rep;
        using units = typename num_traits<T>::units;
        using factor = typename num_traits<T>::factor;
};

template<number T, size_t N> struct num_traits<Box<T, N>> {
        using rep = typename num_traits<T>::rep;
        using units = typename num_traits<T>::units;
        using factor = typename num_traits<T>::factor;
};

#include "geom/operators.h"

template<number To, number From>
constexpr To down_cast(const From& n)
{
        using quotient = std::ratio_divide<num_factor_t<From>, num_factor_t<To>>;
        auto r = get(n) * quotient::num;
        if (r >= 0) return To(r / quotient::den);
        return To(r / quotient::den - !!(r % quotient::den));
}

template<number To, number From>
constexpr Interval<To> down_cast(const Interval<From>& n)
{ return Interval<To>(down_cast<To>(n.min), down_cast<To>(n.max)); }

template<number To, number From, size_t N>
constexpr Vec<To, N> down_cast(const Vec<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Vec<To, N>(down_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

template<number To, number From, size_t N>
constexpr Box<To, N> down_cast(const Box<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Box<To, N>(down_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

template<number To, number From>
constexpr To up_cast(const From& n)
{
        using quotient = std::ratio_divide<num_factor_t<From>, num_factor_t<To>>;
        auto r = get(n) * quotient::num;
        if (r <= 0) return To(r / quotient::den);
        return To(r / quotient::den + !!(r % quotient::den));
}

template<number To, number From>
constexpr Interval<To> up_cast(const Interval<From>& n)
{ return Interval<To>(up_cast<To>(n.min), up_cast<To>(n.max)); }

template<number To, number From, size_t N>
constexpr Vec<To, N> up_cast(const Vec<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Vec<To, N>(up_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

template<number To, number From, size_t N>
constexpr Box<To, N> up_cast(const Box<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Box<To, N>(up_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

template<number To, number From>
constexpr Interval<To> in_cast(const Interval<From>& n)
{ return Interval<To>(up_cast<To>(n.min), down_cast<To>(n.max)); }

template<number To, number From, size_t N>
constexpr Vec<To, N> in_cast(const Vec<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Vec<To, N>(in_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

template<number To, number From, size_t N>
constexpr Box<To, N> in_cast(const Box<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Box<To, N>(in_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

template<number To, number From>
constexpr Interval<To> out_cast(const Interval<From>& n)
{ return Interval<To>(down_cast<To>(n.min), up_cast<To>(n.max)); }

template<number To, number From, size_t N>
constexpr Vec<To, N> out_cast(const Vec<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Vec<To, N>(out_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

template<number To, number From, size_t N>
constexpr Box<To, N> out_cast(const Box<From, N>& n)
{
        return [&n]<size_t... I>(std::index_sequence<I...>)
        { return Box<To, N>(out_cast<To>(n[I])...); }
        (std::make_index_sequence<N>{});
}

struct contains_fn {
        template<class T, class U>
        constexpr bool operator()(const Interval<T>& i, const U& u) const
        { return i.min <= u && i.max >= u; }

        template<class T, class U>
        constexpr bool operator()(const Interval<T>& i, const Interval<U>& j) const
        { return i.min <= j.min && i.max >= j.max; }

        template<class T, class U, size_t N>
        constexpr bool operator()(const Box<T, N>& b, const Vec<U, N>& p) const
        {
                auto i = begin(b);
                auto s = end(b);
                auto j = begin(p);
                while (i != s)
                        if (!(*this)(*i++, *j++)) return false;
                return true;
        }

        template<class T, class U, size_t N>
        constexpr bool operator()(const Box<T, N>& b, const Box<U, N>& p) const
        {
                auto i = begin(b);
                auto s = end(b);
                auto j = begin(p);
                while (i != s)
                        if (!(*this)(*i++, *j++)) return false;
                return true;
        }
};

inline constexpr contains_fn contains;

struct intersects_fn {
        template<class T, class U>
        constexpr bool operator()(const Interval<T>& i, const Interval<U>& j) const
        { return i.max >= j.min && j.max >= i.min; }

        template<class T, class U, size_t N>
        constexpr bool operator()(const Box<T, N>& l, const Box<U, N>& r) const
        {
                auto i = begin(l);
                auto s = end(l);
                auto j = begin(r);
                while (i != s)
                        if (!(*this)(*i++, *j++)) return false;
                return true;
        }
};

inline constexpr intersects_fn intersects;

struct intersection_fn {
        template<class T, class U>
        constexpr auto operator()(const Interval<T>& i, const Interval<U>& j) const
        { return make_interval(std::max(i.min, j.min), std::min(i.max, j.max)); }

        template<class T, class U, size_t N>
        constexpr auto operator()(const Box<T, N>& l, const Box<U, N>& r) const
        {
                return [&]<size_t... I>(std::index_sequence<I...>)
                { return make_box((*this)(l[I], r[I])...); }
                (std::make_index_sequence<N>{});
        }
};

inline constexpr intersection_fn intersection;

struct join_fn {
        template<class T, class U>
        constexpr auto operator()(const Interval<T>& i, const Interval<U>& j) const
        { return make_interval(std::min(i.min, j.min), std::max(i.max, j.max)); }

        template<class T, class U, size_t N>
        constexpr auto operator()(const Box<T, N>& l, const Box<U, N>& r) const
        {
                return [&]<size_t... I>(std::index_sequence<I...>)
                { return make_box((*this)(l[I], r[I])...); }
                (std::make_index_sequence<N>{});
        }
};

inline constexpr join_fn join;

template<class Ratio, number T>
constexpr auto point(const Interval<T>& i)
{ return i.min + (i.max - i.min) * Ratio::num / Ratio::den; }

template<class... Ratios, number T>
constexpr auto point(const Box<T, sizeof...(Ratios)>& b)
{
        return [&b]<size_t... I>(std::index_sequence<I...>)
        { return make_vec(point<Ratios>(b[I])...); }
        (std::make_index_sequence<sizeof...(Ratios)>{});
}

using min = std::ratio<0>;
using mid = std::ratio<1, 2>;
using max = std::ratio<1>;

template<number T> constexpr auto top_left(const Box<T, 2>& b)
{ return point<min, min>(b); }
template<number T> constexpr auto top(const Box<T, 2>& b)
{ return point<mid, min>(b); }
template<number T> constexpr auto top_right(const Box<T, 2>& b)
{ return point<max, min>(b); }
template<number T> constexpr auto left(const Box<T, 2>& b)
{ return point<min, mid>(b); }
template<number T> constexpr auto center(const Box<T, 2>& b)
{ return point<mid, mid>(b); }
template<number T> constexpr auto right(const Box<T, 2>& b)
{ return point<max, mid>(b); }
template<number T> constexpr auto bottom_left(const Box<T, 2>& b)
{ return point<min, max>(b); }
template<number T> constexpr auto bottom(const Box<T, 2>& b)
{ return point<mid, max>(b); }
template<number T> constexpr auto bottom_right(const Box<T, 2>& b)
{ return point<max, max>(b); }

template<number T> constexpr auto front_top_left(const Box<T, 3>& b)
{ return point<min, min, min>(b); }
template<number T> constexpr auto front_top(const Box<T, 3>& b)
{ return point<mid, min, min>(b); }
template<number T> constexpr auto front_top_right(const Box<T, 3>& b)
{ return point<max, min, min>(b); }
template<number T> constexpr auto front_left(const Box<T, 3>& b)
{ return point<min, mid, min>(b); }
template<number T> constexpr auto front(const Box<T, 3>& b)
{ return point<mid, mid, min>(b); }
template<number T> constexpr auto front_right(const Box<T, 3>& b)
{ return point<max, mid, min>(b); }
template<number T> constexpr auto front_bottom_left(const Box<T, 3>& b)
{ return point<min, max, min>(b); }
template<number T> constexpr auto front_bottom(const Box<T, 3>& b)
{ return point<mid, max, min>(b); }
template<number T> constexpr auto front_bottom_right(const Box<T, 3>& b)
{ return point<max, max, min>(b); }

template<number T> constexpr auto top_left(const Box<T, 3>& b)
{ return point<min, min, mid>(b); }
template<number T> constexpr auto top(const Box<T, 3>& b)
{ return point<mid, min, mid>(b); }
template<number T> constexpr auto top_right(const Box<T, 3>& b)
{ return point<max, min, mid>(b); }
template<number T> constexpr auto left(const Box<T, 3>& b)
{ return point<min, mid, mid>(b); }
template<number T> constexpr auto center(const Box<T, 3>& b)
{ return point<mid, mid, mid>(b); }
template<number T> constexpr auto right(const Box<T, 3>& b)
{ return point<max, mid, mid>(b); }
template<number T> constexpr auto bottom_left(const Box<T, 3>& b)
{ return point<min, max, mid>(b); }
template<number T> constexpr auto bottom(const Box<T, 3>& b)
{ return point<mid, max, mid>(b); }
template<number T> constexpr auto bottom_right(const Box<T, 3>& b)
{ return point<max, max, mid>(b); }

template<number T> constexpr auto back_top_left(const Box<T, 3>& b)
{ return point<min, min, max>(b); }
template<number T> constexpr auto back_top(const Box<T, 3>& b)
{ return point<mid, min, max>(b); }
template<number T> constexpr auto back_top_right(const Box<T, 3>& b)
{ return point<max, min, max>(b); }
template<number T> constexpr auto back_left(const Box<T, 3>& b)
{ return point<min, mid, max>(b); }
template<number T> constexpr auto back(const Box<T, 3>& b)
{ return point<mid, mid, max>(b); }
template<number T> constexpr auto back_right(const Box<T, 3>& b)
{ return point<max, mid, max>(b); }
template<number T> constexpr auto back_bottom_left(const Box<T, 3>& b)
{ return point<min, max, max>(b); }
template<number T> constexpr auto back_bottom(const Box<T, 3>& b)
{ return point<mid, max, max>(b); }
template<number T> constexpr auto back_bottom_right(const Box<T, 3>& b)
{ return point<max, max, max>(b); }

template<geom::vector T>
bool step(T& pos, const T& min, const T& max)
{
        auto i = std::ranges::begin(pos);
        auto j = std::ranges::begin(min);
        auto k = std::ranges::begin(max);
        while (++*i == *k) {
                *i = *j;
                ++i; ++j; ++k;
                if (i == std::ranges::end(pos))
                        return true;
        }
        return false;
}

template<geom::vector T>
class Frame : public std::ranges::view_interface<Frame<T>> {
 private:
        T _min;
        T _max;

 public:
        struct sentinel {};

        class iterator {
         private:
                T _min;
                T _max;
                T _pos;
                bool _done{false};

         public:
                void next() { _done = _done || step(_pos, _min, _max); }

                constexpr iterator(const T min, const T max, const T pos)
                        : _min{min}, _max{max}, _pos{pos} {}

                constexpr iterator() = default;

                constexpr const T& operator*() const { return _pos; }
                constexpr iterator& operator++() { next(); return *this; }

                constexpr bool operator==(const iterator& r) const { return _pos == r._pos; }
                constexpr bool operator==(sentinel) const { return _done; }
        };

        constexpr Frame() = default;
        constexpr Frame(const T& min, const T& max) : _min{min}, _max{max} {}

        constexpr iterator begin() const { return iterator{_min, _max, _min}; }
        constexpr sentinel end() const { return sentinel{}; }
};

struct frame_fn {
        template<geom::number A, geom::number B, size_t N>
        constexpr Frame<Vec<A, N>> operator()(const Vec<A, N>& min, const Vec<B, N>& max) const
        { return Frame<Vec<A, N>>{min, max}; }

        template<geom::box T>
        constexpr auto operator()(const T& box) const
        { return (*this)(box.min(), box.max()); }
};

inline constexpr frame_fn frame;

struct Vec_hash {
        static constexpr const size_t _p1 = 73856093;
        static constexpr const size_t _p2 = 19349663;
        static constexpr const size_t _p3 = 83492791;

        template<number T>
        constexpr size_t operator()(const Vec<T, 1>& t) const
        { return (_p1 * t.x); }

        template<number T>
        constexpr size_t operator()(const Vec<T, 2>& t) const
        { return (_p1 * t.x) ^ (_p2 * t.y); }

        template<number T>
        constexpr size_t operator()(const Vec<T, 3>& t) const
        { return (_p1 * t.x) ^ (_p2 * t.y) ^ (_p3 * t.z); }
};

struct Cantor_hash {
        template<number T, size_t N>
        constexpr size_t operator()(const Vec<T, N>& t) const
        {
                auto i = std::ranges::begin(t);
                auto s = std::ranges::end(t);
                size_t a = (size_t)*i;
                while (++i != s) {
                        size_t b = (size_t)*i;
                        a = (b * b + 3 * b + 2 * b * a + a + a * a) / 2;
                }
                return a;
        }
};

struct Szudzik_hash {
        template<number T>
        constexpr size_t operator()(const Vec<T, 1>& t) const
        {
                return t.x;
        }

        template<number T>
        constexpr size_t operator()(const Vec<T, 2>& t) const
        {
                auto a = (size_t)t.x;
                auto b = (size_t)t.y;
                if (b < a) return a * a + b;
                else return b * b + b + a;
        }

        template<number T>
        constexpr size_t operator()(const Vec<T, 3>& t) const
        {
                auto a = (size_t)t.x;
                auto b = (size_t)t.y;
                auto c = (size_t)t.z;
                if (b >= a) {
                        if (c >= b) return c*c*c + 2*c*c + c + b*b + b + a;
                        else return b*b*b + 2*b*c + c + b + a;
                } else {
                        if (c >= a) return c*c*c + 2*c*c + c + a*a + b;
                        else return a*a*a + 2*c*a + c + b;
                }
        }
};

}; // namespace geom

#include <ostream>

namespace geom {

template<geom::interval T> std::ostream& operator<<(std::ostream& os, const T& t)
{ return os << '(' << t.min << ' ' << t.max << ')'; }

template<geom::vector T> std::ostream& operator<<(std::ostream& os, const T& t)
{
        os << '(' << t[0];
        for (const auto& e : t | std::views::drop(1))
                os << ' ' << e;
        return os << ')';
}

template<geom::box T> std::ostream& operator<<(std::ostream& os, const T& t)
{
        os << '(' << t[0];
        for (const auto& e : t | std::views::drop(1))
                os << ' ' << e;
        return os << ')';
}

}; // namespace geom

namespace std {

template<geom::interval T> string to_string(const T& t)
{ return '(' + to_string(t.min) + ' ' + to_string(t.max) + ')'; }

template<geom::vector T> string to_string(const T& t)
{
        string r;
        r += '(' += to_string(t[0]);
        for (const auto& e : t | views::drop(1))
                r += ' ' += to_string(e);
        return r += ')';
}

template<geom::box T> string to_string(const T& t)
{
        string r;
        r += '(' += to_string(t[0]);
        for (const auto& e : t | views::drop(1))
                r += ' ' += to_string(e);
        return r += ')';
}

}; // namespace std

#endif // GEOM_MATH_H_
