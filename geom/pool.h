// Storage of elements to be referenced by index.

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Geom.

// Geom is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Geom is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Geom.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GEOM_POOL_H_
#define GEOM_POOL_H_

#include <vector>
#include <bitset>
#include <type_traits>
#include <iterator>

namespace geom {

// A pool is an allocating mechanism that produces elements that are identified
// by index instead of by pointer.

template<class T> struct Pool {
 public:
        using value_type = T;
        using reference = T&;
        using const_reference = const T&;
        using pointer = T*;
        using const_pointer = const T*;
        using size_type = size_t;

 private:
        union Elem {
                size_type next;
                std::aligned_storage_t<sizeof(T), alignof(T)> value;
        };

        struct Chunk {
                std::bitset<8> used;
                Elem elems[8];

                void destroy_()
                {
                        for (int i = 0; i < 8; ++i) if (used[i])
                                reinterpret_cast<T*>(&elems[i])->~T();
                }

                void copy_(const Chunk& r)
                {
                        used = r.used;
                        for (int i = 0; i < 8; ++i) if (used[i])
                                new(&elems[i]) T(*reinterpret_cast<const T*>(&r.elems[i]));
                }

                void move_(Chunk&& r)
                {
                        used = r.used;
                        for (int i = 0; i < 8; ++i) if (used[i])
                                new(&elems[i]) T(std::move(*reinterpret_cast<T*>(&r.elems[i])));
                }

                Chunk() = default;
                Chunk(const Chunk& r) { copy_(r); }
                Chunk(Chunk&& r) { move_(std::move(r)); }

                ~Chunk() { destroy_(); }

                Chunk& operator=(const Chunk& r)
                { destroy_(); copy_(r); return *this; }
                Chunk& operator=(Chunk&& r)
                { destroy_(); move_(std::move(r)); return *this; }
        };

        std::vector<Chunk> chunks;
        size_type next{0};
        size_type top{0};

        static size_type next_used_(std::vector<Chunk>& chunks, size_type pos)
        {
                size_type n = pos / 8;
                size_type m = pos % 8;
                while (n < chunks.size()) {
                        while (m < 8) {
                                if (chunks[n].used[m]) return n * 8 + m;
                                ++m;
                        }
                        m = 0;
                        ++n;
                }
                return n * 8;
        }

 public:
        template<class Base, class Ref, class Ptr> struct Iterator_base {
                Base chunks;
                size_type pos;

                Ptr get_() { return reinterpret_cast<Ptr>(&chunks[pos / 8].elems[pos % 8]); }

                using value_type = T;
                using reference = Ref;
                using pointer = Ptr;
                using difference_type = ptrdiff_t;
                using iterator_category = std::forward_iterator_tag;

                reference operator*() { return *get_(); }
                pointer operator->() { return get_(); }

                Iterator_base& operator++() { pos = next_used_(chunks, pos + 1); return *this; }
                Iterator_base operator++(int) { auto r = *this; ++*this; return r; }

                bool operator==(const Iterator_base& other) { return pos == other.pos; }
                bool operator!=(const Iterator_base& other) { return pos != other.pos; }
        };

        using iterator = Iterator_base<std::vector<Chunk>&, T&, T*>;
        using const_iterator = Iterator_base<const std::vector<Chunk>&, const T&, const T*>;

        size_type insert(const_reference value)
        {
                size_type ret = next;
                if (next == top) {
                        if (next == chunks.size() * 8) chunks.push_back({});
                        ++next;
                        ++top;
                } else {
                        next = chunks[next / 8].elems[next % 8].next;
                }
                Chunk& chunk = chunks[ret / 8];
                size_t off = ret % 8;
                new (&chunk.elems[off]) T(value);
                chunk.used[off] = true;
                return ret;
        }

        void erase(size_type pos)
        {
                Chunk& chunk = chunks[pos / 8];
                size_t off = pos % 8;
                reinterpret_cast<T*>(&chunk.elems[off])->~T();
                chunk.elems[off].next = next;
                chunk.used[off] = false;
                next = pos;
        }

        void clear()
        {
                chunks.clear();
                next = 0;
                top = 0;
        }

        bool empty() const { return begin() == end(); }

        iterator begin() { return iterator{chunks, next_used_(chunks, 0)}; }
        const_iterator begin() const { return const_iterator{chunks, next_used_(chunks, 0)}; }
        const_iterator cbegin() const { return const_iterator{chunks, next_used_(chunks, 0)}; }

        iterator end() { return iterator{chunks, chunks.size() * 8}; }
        const_iterator end() const { return const_iterator{chunks, chunks.size() * 8}; }
        const_iterator cend() const { return const_iterator{chunks, chunks.size() * 8}; }

        reference operator[](size_type pos) { return *iterator{chunks, pos}; }
        const_reference operator[](size_type pos) const { return *const_iterator{chunks, pos}; }

        bool contains(size_type pos)
        { return pos < chunks.size() * 8 && chunks[pos / 8].used[pos % 8]; }

        reference at(size_type pos)
        {
                if (contains(pos)) return *iterator{chunks, pos};
                throw std::out_of_range{"geom::Pool accessed out of range"};
        }
        const_reference at(size_type pos) const
        {
                if (contains(pos)) return *const_iterator{chunks, pos};
                throw std::out_of_range{"geom::Pool accessed out of range"};
        }
};

// When elements in the pool are trivial, constructors and destructors can be
// ignored and no metadata is required.

template<class T> requires std::is_trivial_v<T> class Pool<T> {
 public:
        using value_type = T;
        using reference = T&;
        using const_reference = const T&;
        using pointer = T*;
        using const_pointer = const T*;
        using size_type = size_t;

 private:
        union Elem {
                T value;
                size_type next;
        };
        std::vector<Elem> data;
        size_type next{0};

 public:
        size_type insert(const_reference value)
        {
                size_type ret = next;
                if (next == data.size()) {
                        ++next;
                        data.push_back({value});
                } else {
                        next = data[next].next;
                        data[ret].value = value;
                }
                return ret;
        }

        void erase(size_type pos)
        {
                data[pos].next = next;
                next = pos;
        }

        reference operator[](size_type pos) { return data[pos].value; }
        const_reference operator[](size_type pos) const { return data[pos].value; }
};

}; // namespace geom

#endif // GEOM_POOL_H_
