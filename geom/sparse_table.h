// Sparse_table - multidimensional table using a tree implementation

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Geom.

// Geom is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Geom is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Geom.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GEOM_SPARSE_TABLE_H_
#define GEOM_SPARSE_TABLE_H_

#include <algorithm>

#include "geom/tree.h"
#include "geom/table.h"

namespace geom {

// Proxy class that behaves like a reference to an element in a sparse
// table.  It may not be pointing to an unique final node, but it'll
// always point to a final node with the correct value, and assigning a
// new value will either create a new node for it, or merge it with its
// neighbours.
//
// Needs to be defined outside Sparse_table because template deduction rules
// would not allow a number of operations to be implemented otherwise.

template<vector Key, class T, class Container> class Sparse_table_ref {
 private:
        Container* _cont{nullptr};
        Key _pos{};

 public:
        using key_type = Key;
        using mapped_type = T;

        constexpr Sparse_table_ref() = default;
        constexpr Sparse_table_ref(Container* cont, Key pos) :
                _cont{cont}, _pos{pos} {}

        constexpr operator const T&() const
        { return _cont->find(_pos); }

        constexpr const Sparse_table_ref& operator=(const T& r) const
        { _cont->insert(_pos, r); return *this; }
};

template<vector Key, class T, class Container = Tree<Key, T>> class Sparse_table {
 public:
        using key_type = Key;
        using mapped_type = T;
        using difference_type = ptrdiff_t;
        using reference = Sparse_table_ref<Key, T, Container>;
        using const_reference = const T&;
        using pointer = T*;
        using const_pointer = const T*;

        struct iterator : public Table_iterator<iterator> {
         private:
                Sparse_table* _table;

         public:
                using Base = Table_iterator<iterator>;
                using difference_type = ptrdiff_t;
                using value_type = T;
                using reference = typename Sparse_table::reference;
                using pointer = void;
                using iterator_category = std::random_access_iterator_tag;

                constexpr Key key() const
                { return _table->offset() + unfold(Base::_pos, _table->size()); }
                constexpr decltype(auto) operator*() const { return (*_table)[key()]; }

                constexpr iterator() = default;
                constexpr iterator(Sparse_table* table, ptrdiff_t pos)
                        : Base{pos}, _table{table} {}
        };

        struct const_iterator : public Table_iterator<const_iterator> {
         private:
                const Sparse_table* _table;

         public:
                using Base = Table_iterator<iterator>;
                using difference_type = ptrdiff_t;
                using value_type = T;
                using reference = typename Sparse_table::const_reference;
                using pointer = void;
                using iterator_category = std::random_access_iterator_tag;

                constexpr Key key() const
                { return _table->offset() + unfold(Base::_pos, _table->size()); }
                constexpr decltype(auto) operator*() const { return (*_table)[key()]; }

                constexpr const_iterator() = default;
                constexpr const_iterator(const Sparse_table* table, ptrdiff_t pos)
                        : Base{pos}, _table{table} {}
                constexpr const_iterator(const iterator& r)
                        : Base{r._pos}, _table{r._table} {}
        };

 private:
        Container _cont;
        key_type _offset;
        key_type _size;
        size_t _cap;

        size_t _calculate_cap(const key_type& s)
        {
                return std::accumulate(std::ranges::begin(s), std::ranges::end(s),
                                1, std::multiplies{});
        }

 public:
        constexpr Sparse_table(const key_type& offset, const key_type& size) :
                _cont((size_t)*std::ranges::max_element(size)),
                _offset{offset},
                _size{size},
                _cap{_calculate_cap(size)}
        { }

        constexpr Sparse_table(const key_type& offset, const key_type& size, const T& value) :
                _cont((size_t)*std::ranges::max_element(size), value),
                _offset{offset},
                _size{size},
                _cap{_calculate_cap(size)}
        { }

        constexpr Sparse_table() = default;

        constexpr Sparse_table& operator=(std::initializer_list<T> ilist)
        { std::copy(ilist.begin(), ilist.end(), begin()); return *this; }

        constexpr reference operator[](const key_type& n) { return reference{&_cont, n - offset()}; }
        constexpr const_reference operator[](const key_type& n) const { return _cont.find(n - offset()); }

        constexpr Container& base() { return _cont; }
        constexpr const Container& base() const { return _cont; }

        constexpr iterator begin() { return iterator(this, 0); }
        constexpr const_iterator begin() const { return const_iterator(this, 0); }
        constexpr const_iterator cbegin() const { return const_iterator(this, 0); }

        constexpr iterator end() { return iterator(this, _cap); }
        constexpr const_iterator end() const { return const_iterator(this, _cap); }
        constexpr const_iterator cend() const { return const_iterator(this, _cap); }

        constexpr const key_type& offset() const { return _offset; }
        constexpr const key_type& size() const { return _size; }
        constexpr size_t capacity() const { return _cap; }

        void resize(const key_type& new_off, const key_type& new_size)
        {
                _cont.resize((size_t)*std::ranges::max_element(new_size));
                _offset = new_off;
                _size = new_size;
                _cap = _calculate_cap(new_size);
        }
        void resize(const key_type& new_off, const key_type& new_size, const T& value)
        {
                _cont.resize((size_t)*std::ranges::max_element(new_size), value);
                _offset = new_off;
                _size = new_size;
                _cap = _calculate_cap(new_size);
        }

        constexpr bool contains(const key_type& n) const
        { return contains(make_box(_offset, _offset + _size), n); }
};

}; // namespace geom

template<geom::vector Key, class T, class Container>
constexpr bool
operator==(const typename geom::Sparse_table_ref<Key, T, Container>& r, const T& t)
{ return (T)r == t; }

#endif // GEOM_SPARSE_TABLE_H_
