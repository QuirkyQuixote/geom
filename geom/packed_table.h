// packed_table.h - implement Packed_table

// Copyright (C) 2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of Geom.

// Geom is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Geom is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Geom.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GEOM_PACKED_TABLE_H_
#define GEOM_PACKED_TABLE_H_

#include <vector>

#include "geom/table.h"

namespace geom {

template<vector Key, class T, class Container = std::vector<T>>
struct Packed_table {
 public:
        using key_type = Key;
        using mapped_type = T;
        using reference = T&;
        using const_reference = const T&;
        using pointer = T*;
        using const_pointer = const T*;

 private:
        Container _cont;
        Key _offset;
        Key _size;

        static constexpr size_t _cont_size(const Key& s)
        {
                size_t r{1};
                for (auto e : s) r *= get(e);
                return r;
        }

 public:
        struct iterator : public Table_iterator<iterator> {
         private:
                Packed_table* _table{nullptr};

         public:
                using Base = Table_iterator<iterator>;
                using difference_type = ptrdiff_t;
                using value_type = T;
                using reference = T&;
                using pointer = T*;
                using iterator_category = std::random_access_iterator_tag;

                constexpr Key key() const
                { return _table->offset() + unfold(Base::_pos, _table->size()); }
                constexpr decltype(auto) operator*() const { return (*_table)[key()]; }

                constexpr iterator(Packed_table* table, ptrdiff_t pos)
                        : Base{pos}, _table{table} {}
                constexpr iterator() = default;
        };

        struct const_iterator : public Table_iterator<const_iterator> {
         private:
                const Packed_table* _table{nullptr};

         public:
                using Base = Table_iterator<const_iterator>;
                using difference_type = ptrdiff_t;
                using value_type = T;
                using reference = const T&;
                using pointer = const T*;
                using iterator_category = std::random_access_iterator_tag;

                constexpr Key key() const
                { return _table->offset() + unfold(Base::_pos, _table->size()); }
                constexpr decltype(auto) operator*() const { return (*_table)[key()]; }

                constexpr const_iterator(const Packed_table* table, ptrdiff_t pos)
                        : Base{pos}, _table{table} {}
                constexpr const_iterator(const iterator& r)
                        : Base{r._pos}, _table{r._table} {}
                constexpr const_iterator() = default;
        };

        constexpr Packed_table(const Key& offset, const Key& size, const T& val = {}) :
                _cont(_cont_size(size), val), _offset{offset}, _size{size} {}

        constexpr Packed_table() = default;

        constexpr Packed_table& operator=(std::initializer_list<T> l)
        { _cont = l; return *this; }

        constexpr iterator begin() { return iterator(this, 0); }
        constexpr const_iterator begin() const { return const_iterator(this, 0); }
        constexpr iterator end() { return iterator(this, _cont.size()); }
        constexpr const_iterator end() const { return const_iterator(this, _cont.size()); }

        constexpr decltype(auto) operator[](const Key& k)
        { return _cont[fold(k - offset(), size())]; }
        constexpr decltype(auto) operator[](const Key& k) const
        { return _cont[fold(k - offset(), size())]; }

        constexpr const Key& offset() const { return _offset; }
        constexpr const Key& size() const { return _size; }
        constexpr size_t capacity() const { return _cont.size(); }

        void resize(const key_type& new_off, const key_type& new_size)
        {
                _cont.resize(_cont_size(new_size));
                _offset = new_off;
                _size = new_size;
        }
        void resize(const key_type& new_off, const key_type& new_size, const T& value)
        {
                _cont.resize(_cont_size(new_size), value);
                _offset = new_off;
                _size = new_size;
        }

        constexpr bool contains(const key_type& n) const
        { return contains(make_box(_offset, _offset + _size), n); }
};

}; // namespace geom

#endif // GEOM_PACKED_TABLE_H_
