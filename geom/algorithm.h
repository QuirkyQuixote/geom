// Table_algorithms - transform tables

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Geom.

// Geom is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Geom is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Geom.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GEOM_ALGORITHMS_H_
#define GEOM_ALGORITHMS_H_

#include <algorithm>

#include "geom/traits.h"
#include "geom/slice.h"

namespace geom {

// Cast number

template<number To, number From>
requires std::same_as<num_units_t<To>, num_units_t<From>>
constexpr To down_cast(const From& n)
{
        using quotient = std::ratio_divide<num_factor_t<From>, num_factor_t<To>>;
        auto r = get(n) * quotient::num;
        if (r >= 0) return To(r / quotient::den);
        return To(r / quotient::den - !!(r % quotient::den));
}

template<number To, number From>
requires std::same_as<num_units_t<To>, num_units_t<From>>
constexpr To up_cast(const From& n)
{
        using quotient = std::ratio_divide<num_factor_t<From>, num_factor_t<To>>;
        auto r = get(n) * quotient::num;
        if (r <= 0) return To(r / quotient::den);
        return To(r / quotient::den + !!(r % quotient::den));
}

// Cast vector

template<number To, vector T> constexpr auto down_cast(const T& x)
{ return transform(x, down_cast<To, typename T::value_type>); }

template<number To, vector T> constexpr auto up_cast(const T& x)
{ return transform(x, up_cast<To, typename T::value_type>); }

// Cast interval

template<number To, interval T> constexpr auto down_cast(const T& x)
{ return make_interval(down_cast<To>(x.min), down_cast<To>(x.max)); }

template<number To, interval T> constexpr auto up_cast(const T& x)
{ return make_interval(up_cast<To>(x.min), up_cast<To>(x.max)); }

template<number To, interval T> constexpr auto in_cast(const T& x)
{ return make_interval(up_cast<To>(x.min), down_cast<To>(x.max)); }

template<number To, interval T> constexpr auto out_cast(const T& x)
{ return make_interval(down_cast<To>(x.min), up_cast<To>(x.max)); }

// Cast box

template<number To, box T> constexpr auto down_cast(const T& x)
{ return transform(x, down_cast<To, typename T::value_type>); }

template<number To, box T> constexpr auto up_cast(const T& x)
{ return transform(x, up_cast<To, typename T::value_type>); }

template<number To, box T> constexpr auto in_cast(const T& x)
{ return transform(x, in_cast<To, typename T::value_type>); }

template<number To, box T> constexpr auto out_cast(const T& x)
{ return transform(x, out_cast<To, typename T::value_type>); }

// Table algorithms

template<table T>
T resize(const T& table, const table_box_t<T>& box)
{
        table_box_t<T> b1 = intersection(box, table.box());
        table_box_t<T> b2 = b1 - box.min();
        T result(box.size());
        std::ranges::move(geom::slice(table, b1), geom::slice(result, b2).begin());
        return result;
}

template<table T>
T grow(const T& table, const table_box_t<T>& box)
{
        T result(table.size() + box.min() + box.max());
        std::ranges::move(table, geom::slice(result, box.min(), table.size()).begin());
        return result;
}

template<table T>
T shrink(const T& table, const table_box_t<T>& box)
{
        T result(table.size() - box.min() - box.max());
        std::ranges::move(geom::slice(table, box.min(), result.size()), result.begin());
        return result;
}

template<table T, std::predicate<const typename T::value_type&> P>
table_box_t<T> shave(const T& table, P pred)
{
        table_size_t<T> min = table.size();
        table_size_t<T> max;
        auto i = table.begin();
        while (i != table.end()) {
                if (!pred(*i)) {
                        auto p = path(i, table.begin());
                        for (size_t n = 0; n != dimension_v<T>; ++n) {
                                if (min[n] > p[n]) min[n] = p[n];
                                if (max[n] < p[n]) max[n] = p[n];
                        }
                }
                ++i;
        }
        return table_box_t<T>(min, ++max);
}

}; // namespace geom

#endif // GEOM_ALGORITHMS_H_


