// Num - type-safe integer arithmetic.

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Geom.

// Geom is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Geom is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Geom.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GEOM_NUM_H_
#define GEOM_NUM_H_

#include <cmath>
#include <numeric>
#include <ratio>

namespace geom {

template<int... N> struct Unit {
        template<int... M> constexpr auto operator+(Unit<M...>) { return Unit<N + M...>{}; }
        template<int... M> constexpr auto operator-(Unit<M...>) { return Unit<N - M...>{}; }
};

template<class F, class F2> using common_ratio_t =
        std::ratio<std::gcd(F::num, F2::num), std::lcm(F::den, F2::den)>;

// Scalar implementation

template<std::integral I, class U, class F = std::ratio<1>> class Num {
 public:
        using rep = I;
        using units = U;
        using factor = F;

 private:
        I _{0};

 public:
        constexpr Num() = default;

        template<std::integral I2>
        constexpr explicit Num(I2 x) : _(x) {}

        template<std::integral I2, class F2>
        constexpr Num(const Num<I2, U, F2>& n)
        {
                using Q = std::ratio_divide<F2, F>;
                _ = I2(n) * Q::num / Q::den;
        }

        template<std::integral I2>
        constexpr explicit operator I2() const { return _; }

        constexpr Num operator+() const { return Num(_); }
        constexpr Num operator-() const { return Num(-_); }

        constexpr Num& operator++() { ++_; return *this; }
        constexpr Num& operator--() { --_; return *this; }
        constexpr Num operator++(int) { Num r = *this; ++_; return r; }
        constexpr Num operator--(int) { Num r = *this; --_; return r; }

        template<std::integral I2, class F2>
        constexpr Num& operator+=(const Num<I2, U, F2>& r)
        { _ += I(Num(r)); return *this; }

        template<std::integral I2, class F2>
        constexpr Num& operator-=(const Num<I2, U, F2>& r)
        { _ -= I(Num(r)); return *this; }

        template<std::integral I2>
        constexpr Num& operator*=(I2 r) { _ *= r; return *this; }

        template<std::integral I2>
        constexpr Num& operator/=(I2 r) { _ /= r; return *this; }

        template<std::integral I2>
        constexpr Num& operator%=(I2 r) { _ %= r; return *this; }

        template<std::integral I2, class U2, class F2>
        requires std::same_as<decltype(U() + U2()), U>
        constexpr Num& operator*=(const Num<I2, U2, F2>& r)
        { _ *= I2(Num<I2, U2, std::ratio<1>>(r)); return *this; }

        template<std::integral I2, class U2, class F2>
        requires std::same_as<decltype(U() - U2()), U>
        constexpr Num& operator/=(const Num<I2, U2, F2>& r)
        { _ /= I2(Num<I2, U2, std::ratio<1>>(r)); return *this; }

        template<std::integral I2, class U2, class F2>
        requires std::same_as<decltype(U() - U2()), U>
        constexpr Num& operator%=(const Num<I2, U2, F2>& r)
        { _ %= I2(Num<I2, U2, std::ratio<1>>(r)); return *this; }

        template<std::integral I2, class F2>
        constexpr auto operator+(const Num<I2, U, F2>& r) const
        {
                using S = Num<decltype(I() + I2()), U, common_ratio_t<F, F2>>;
                return S(*this) += S(r);
        }

        template<std::integral I2, class F2>
        constexpr auto operator-(const Num<I2, U, F2>& r) const
        {
                using S = Num<decltype(I() - I2()), U, common_ratio_t<F, F2>>;
                return S(*this) -= S(r);
        }

        template<std::integral I2, class U2, class F2>
        constexpr auto operator*(const Num<I2, U2, F2>& r) const
        {
                using S = Num<decltype(I() * I2()), decltype(U() + U2()),
                      std::ratio_multiply<F, F2>>;
                return S(_ * I2(r));
        }

        template<std::integral I2, class U2, class F2>
        constexpr auto operator/(const Num<I2, U2, F2>& r) const
        {
                using S = Num<decltype(I() / I2()), decltype(U() - U2()),
                      std::ratio_divide<F, F2>>;
                return S(_ / I2(r));
        }

        template<std::integral I2, class U2, class F2>
        constexpr auto operator%(const Num<I2, U2, F2>& r) const
        {
                using S = Num<decltype(I() % I2()), decltype(U() - U2()),
                      std::ratio_divide<F, F2>>;
                return S(_ % I2(r));
        }

        template<std::integral I2>
        constexpr auto operator*(I2 r) const
        { return Num<decltype(I() * I2()), U, F>(_ * r); }

        template<std::integral I2>
        constexpr auto operator/(I2 r) const
        { return Num<decltype(I() / I2()), U, F>(_ / r); }

        template<std::integral I2>
        constexpr auto operator%(I2 r) const
        { return Num<decltype(I() % I2()), U, F>(_ % r); }

        template<std::integral I2, class F2>
        constexpr auto operator<=>(const Num<I2, U, F2>& r) const
        {
                using R = std::common_type_t<I, I2>;
                using C = Num<R, U, common_ratio_t<F, F2>>;
                return R(C(*this)) <=> R(C(r));
        }

        template<std::integral I2, class F2>
        constexpr bool operator==(const Num<I2, U, F2>& r) const
        { return (*this <=> r) == 0; }

        template<std::integral I2, class F2>
        constexpr bool operator!=(const Num<I2, U, F2>& r) const
        { return (*this <=> r) != 0; }

        template<std::integral I2, class F2>
        constexpr bool operator<(const Num<I2, U, F2>& r) const
        { return (*this <=> r) < 0; }

        template<std::integral I2, class F2>
        constexpr bool operator<=(const Num<I2, U, F2>& r) const
        { return (*this <=> r) <= 0; }

        template<std::integral I2, class F2>
        constexpr bool operator>(const Num<I2, U, F2>& r) const
        { return (*this <=> r) > 0; }

        template<std::integral I2, class F2>
        constexpr bool operator>=(const Num<I2, U, F2>& r) const
        { return (*this <=> r) >= 0; }
};

struct get_fn {
        template<std::integral I> constexpr I operator()(I i) const { return i; }

        template<std::integral I, class U, class F>
        constexpr I operator()(Num<I, U, F> i) const { return I(i); }
};

inline constexpr get_fn get;

// Checks whether a type is Num

template<class T> struct num_traits {};

template<std::integral T> struct num_traits<T> {
        using rep = T;
        using units = void;
        using factor = std::ratio<1, 1>;
};

template<std::integral T, class U, class F> struct num_traits<Num<T, U, F>> {
        using rep = T;
        using units = U;
        using factor = F;
};

template<class T> using num_rep_t = num_traits<T>::rep;
template<class T> using num_units_t = num_traits<T>::units;
template<class T> using num_factor_t = num_traits<T>::factor;

template<class T> struct is_number : std::integral_constant<bool, false> {};
template<std::integral I, class U, class F>
struct is_number<Num<I, U, F>> : std::integral_constant<bool, true> {};
template<class T> inline constexpr bool is_number_v = is_number<T>::value;
template<class T> concept number = std::integral<T> || is_number_v<T>;

template<number T> T abs(const T& n) { return T(std::abs(get(n))); }

}; // namespace geom

#include <ostream>

template<std::integral I, class U, class F>
inline std::ostream& operator<<(std::ostream& stream, const geom::Num<I, U, F>& i)
{ return stream << (long long)i; }

namespace std {
template<integral I, class U, class F>
inline string to_string(const geom::Num<I, U, F>& i)
{ return to_string((long long)i); }
};

#endif // GEOM_NUM_H_
