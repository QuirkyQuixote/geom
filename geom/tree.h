// Generalized space-partitioning tree (quadtree/octree/...)

// Copyright (C) 2021 Luis Sanz <luis.sanz@gmail.com>

// This file is part of Geom.

// Geom is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Geom is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Geom.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GEOM_TREE_H_
#define GEOM_TREE_H_

#include <type_traits>
#include <iostream>

#include "geom/pool.h"
#include "geom/math.h"

namespace geom {

// By default, values for a quadtree are stored in a separate list whose
// indices are referenced by the inner nodes. 
// Here the indices are differentiated from inner node indices by being
// negative; the actual final node index is obtained by negating the handle.
// Final nodes are reference counted.

template<class Handle, class T>
class Tree_leaves {
 public:
        using node_handle = Handle;
        using value_type = T;
        using size_type = size_t;

 private:
        struct _finalnode {
                size_type ref_count;
                T value;
        };

        Pool<_finalnode> _final;

 public:
        Handle insert(const T& value)
        { return ~static_cast<Handle>(_final.insert({1, value})); }

        Handle clone(Handle pos)
        {
                ++_final[~pos].ref_count;
                return pos;
        }

        void erase(Handle pos)
        {
                _finalnode& fn = _final[~pos];
                if (--fn.ref_count == 0) _final.erase(~pos);
        }

        const T& operator[](Handle pos) const { return _final[~pos].value; }
};

// When the type of stored data is trivial and of size smaller than the node
// handle, it's possible to store it into the handle itself. In this case, the
// final node list object is just a dummy class that doesn't contain a list,
// just extracts the values from the handles.

template<class Handle, class T>
requires std::is_trivial_v<T> && (sizeof(T) < sizeof(Handle))
class Tree_leaves<Handle, T> {
 public:
        using node_handle = Handle;
        using value_type = T;
        using size_type = size_t;

        Handle insert(const T& value) { return ~static_cast<Handle>(value); }
        Handle clone(Handle pos) { return pos; }
        void erase(Handle pos) {}
        const T& operator[](Handle pos) const { return static_cast<T>(~pos); }
};

// The number of children on an inner node are two to the power of the number
// of dimension of the key.  I don't think there's a simpler way to write that
// with template metaprogramming.

template<vector Key> struct Tree_base {};

template<class N> struct Tree_base<Vec<N, 2>> {
        using key_type = Vec<N, 2>;

        static constexpr const size_t node_size() { return 4; }

        size_t index(size_t depth, const key_type& key) const
        {
                size_t m = 1 << (depth - 1);
                return ((m & (size_t)key.x) ? 1 : 0) + ((m & (size_t)key.y) ? 2 : 0);
        }
};

template<class N> struct Tree_base<Vec<N, 3>> {
        using key_type = Vec<N, 3>;

        static constexpr const size_t node_size() { return 8; }

        size_t index(size_t depth, const key_type& key) const
        {
                size_t m = 1 << (depth - 1);
                return ((m & (size_t)key.x) ? 1 : 0) + ((m & (size_t)key.y) ? 2 : 0) + ((m & (size_t)key.z) ? 4 : 0);
        }
};

// Actual tree implementation

template<vector Key, class T> struct Tree : public Tree_base<Key> {
 public:
        using Base = Tree_base<Key>;
        using key_type = Key;
        using value_type = T;
        using reference = T&;
        using const_reference = const T&;
        using pointer = T*;
        using const_pointer = const T*;
        using size_type = size_t;

 private:
        using node_handle = int32_t;

        struct _innernode {
                node_handle child[Base::node_size()];
        };

        Pool<_innernode> _inner;
        Tree_leaves<node_handle, T> _final;
        node_handle _root{0};
        node_handle _depth{0};

        void _copy(const Tree& other)
        {
                _inner = other._inner;
                _final = other._final;
                _root = other._root;
                _depth = other._depth;
        }

        void _swap(Tree& other)
        {
                std::swap(_inner, other._inner);
                std::swap(_final, other._final);
                std::swap(_root, other._root);
                std::swap(_depth, other._depth);
        }

        node_handle _replace(node_handle pos, const value_type& value)
        {
                node_handle ret = _final.insert(value);
                _final.erase(pos);
                return ret;
        }

        void _destroy(node_handle pos)
        {
                if (pos < 0) {
                        _final.erase(pos);
                } else {
                        _innernode& in = _inner[pos];
                        for (auto c : in.child) _destroy(c);
                        _inner.erase(pos);
                }
        }

        node_handle _insert_child(node_handle pos, size_type depth,
                        const key_type& key, const value_type& value)
        {
                auto i = Base::index(depth, key);
                // Note that calling _insert() may invalidate the address
                // returned by _inner[pos] and that of its child table, so we
                // can't store any of the pointers elsewhere.
                _inner[pos].child[i] = _insert(_inner[pos].child[i], depth - 1, key, value);
                return pos;
        }

        node_handle _insert(node_handle pos, size_type depth,
                        const key_type& key, const value_type& value)
        {
                if (pos < 0) {
                        if (_final[pos] == value) return pos;
                        if (depth == 0) return _replace(pos, value);
                        return _insert_child(_split(pos), depth, key, value);
                }
                return _try_merge(_insert_child(pos, depth, key, value));
        }

        node_handle _try_merge(node_handle pos)
        {
                _innernode& in = _inner[pos];
                for (auto c : in.child) if (c >= 0) return pos;
                auto ch0 = in.child[0];
                for (size_t i = 1; i < Base::node_size(); ++i)
                        if (ch0 != in.child[i]) return pos;
                node_handle ret = _final.clone(ch0);
                _destroy(pos);
                return ret;
        }

        node_handle _split(node_handle pos)
        {
                _innernode in;
                in.child[0] = pos;
                for (size_t i = 1; i < Base::node_size(); ++i)
                        in.child[i] = _final.clone(pos);
                return _inner.insert(in);
        }

        void _contract(size_type levels)
        {
                for (; levels; --levels) {
                        if (_root < 0) return;
                        node_handle old_root = _root;
                        _innernode& in = _inner[_root];
                        _root = in.child[0];
                        for (size_t i = 1; i < Base::node_size(); ++i) _destroy(in.child[i]);
                        _inner.erase(old_root);
                }
        }

        void _expand(size_type levels)
        {
                if (_root == ~0) return;
                for (; levels; --levels) {
                        _innernode in;
                        in.child[0] = _root;
                        for (size_t i = 1; i < Base::node_size(); ++i)
                                in.child[i] = _final.clone(~0);
                        _root = _inner.insert(in);
                }
        }

        constexpr std::ostream& _print(std::ostream& os, node_handle pos) const
        {
                if (pos < 0) {
                        return os << _final[pos];
                }
                const char* separator = "(";
                for (auto c : _inner[pos].child) {
                        os << separator;
                        _print(os, c);
                        separator = " ";
                }
                return os << ")";
        }

        constexpr size_t _calculate_depth(size_t size) const
        {
                size_t d = 1;
                for (size /= 2; size; size /= 2) ++d;
                return d;
        }

 public:
        Tree() = default;

        Tree(const Tree& r) { _copy(r); }
        Tree(Tree&& r) { _swap(r); }

        Tree(size_type size) : Tree(size, value_type()) {}
        Tree(size_type size, const value_type& value)
        {
                _root = _final.insert(value);
                _depth = _calculate_depth(size);
        }

        ~Tree() { if (_root != 0) _destroy(_root); }

        Tree& operator=(const Tree& r) { _copy(r); return *this; }
        Tree& operator=(Tree&& r) { _swap(r); return *this; }

        const_reference find(const key_type& key)
        {
                node_handle pos = _root;
                for (size_type depth = _depth;; --depth) {
                        if (pos < 0) return _final[pos];
                        pos = _inner[pos].child[Base::index(depth, key)];
                }
        }

        void insert(const key_type& key, const value_type& value)
        { _root = _insert(_root, _depth, key, value); }

        void resize(size_type size)
        {
                auto old_depth = _depth;
                _depth = _calculate_depth(size);
                if (_depth < old_depth) _contract(old_depth - _depth);
                else if (_depth > old_depth) _expand(_depth - old_depth);
        }
};

}; // namespace geom

#endif // GEOM_TREE_H_
