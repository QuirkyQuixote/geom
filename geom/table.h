
#ifndef GEOM_TABLE_H_
#define GEOM_TABLE_H_

#include "geom/math.h"

namespace geom {

template<class T> using table_key_t = typename T::key_type;
template<class T> using table_mapped_t = typename T::mapped_type;

template<class T> concept table = std::ranges::range<T> && vector<table_key_t<T>>;

struct fold_fn {
        template<number A, number B, size_t N>
        constexpr size_t operator()(const Vec<A, N>& key, const Vec<B, N>& size) const
        {
                size_t n = 0;
                switch (N) {
                 case 3:
                        n += geom::get(key[2]);
                        n *= geom::get(size[1]);
                 case 2:
                        n += geom::get(key[1]);
                        n *= geom::get(size[0]);
                 case 1:
                        n += geom::get(key[0]);
                }
                return n;
        }
};

inline constexpr fold_fn fold;

struct unfold_fn {
        template<vector Key>
        constexpr Key operator()(size_t n, const Key& size) const
        {
                using N = geom_value_t<Key>;
                Key k;
                if constexpr (geom_size_v<Key> >= 1) {
                        k[0] = (N)(n % (size_t)size[0]);
                }
                if constexpr (geom_size_v<Key> >= 2) {
                        n /= (size_t)size[0];
                        k[1] = (N)(n % (size_t)size[1]);
                }
                if constexpr (geom_size_v<Key> >= 3) {
                        n /= (size_t)size[1];
                        k[2] = (N)(n % (size_t)size[2]);
                }
                return k;
        }
};

inline constexpr unfold_fn unfold;

template<class Impl> struct Table_iterator {
 protected:
        constexpr Impl& impl() { return *reinterpret_cast<Impl*>(this); }
        constexpr const Impl& impl() const { return *reinterpret_cast<const Impl*>(this); }

        ptrdiff_t _pos{0};

 public:
        constexpr Table_iterator(ptrdiff_t pos) : _pos{pos} {}
        constexpr Table_iterator() = default;
        constexpr Table_iterator(const Table_iterator&) = default;
        constexpr Table_iterator(Table_iterator&&) = default;
        constexpr Table_iterator& operator=(const Table_iterator&) = default;
        constexpr Table_iterator& operator=(Table_iterator&&) = default;

        constexpr decltype(auto) operator[](ptrdiff_t n) const { return *(impl() + n); }

        constexpr Impl& operator++() { ++_pos; return impl(); }
        constexpr Impl& operator--() { --_pos; return impl(); }
        constexpr Impl operator++(int) { auto r = impl(); ++_pos; return r; }
        constexpr Impl operator--(int) { auto r = impl(); --_pos; return r; }

        constexpr Impl& operator+=(ptrdiff_t n) { _pos += n; return impl(); }
        constexpr Impl& operator-=(ptrdiff_t n) { _pos -= n; return impl(); }
        constexpr Impl operator+(ptrdiff_t n) const { return Impl{impl()} += n; }
        constexpr Impl operator-(ptrdiff_t n) const { return Impl{impl()} -= n; }
        constexpr ptrdiff_t operator-(const Impl& r) const { return _pos - r._pos; }

        constexpr bool operator==(const Table_iterator& r) const
        { return _pos == r._pos; }

        constexpr bool operator!=(const Table_iterator& r) const
        { return _pos != r._pos; }

        constexpr auto operator<=>(const Table_iterator& r) const
        { return _pos <=> r._pos; }

        friend struct path_fn;
};

template<class Impl>
constexpr auto operator+(ptrdiff_t n, const Table_iterator<Impl>& l)
{ return l + n; }

struct path_fn {
        template<class Impl>
        constexpr auto operator()(const Table_iterator<Impl>& i,
                        const Table_iterator<Impl>& j) const
        { return j.impl().key() - i.impl().key(); }
};

inline constexpr path_fn path;

template<table T> struct Slice {
 public:
        using key_type = typename T::key_type;
        using mapped_type = typename T::mapped_type;
        using reference = decltype(std::declval<T>()[std::declval<key_type>()]);
        using pointer = mapped_type*;

 private:
        T& _base;
        key_type _offset;
        key_type _size;
        size_t _cap;

 public:
        struct iterator : public Table_iterator<iterator> {
         private:
                const Slice* _slice{nullptr};

         public:
                using Base = Table_iterator<iterator>;
                using difference_type = ptrdiff_t;
                using value_type = typename T::mapped_type;
                using reference = typename T::reference;
                using pointer = typename T::pointer;
                using iterator_category = std::random_access_iterator_tag;

                constexpr key_type key() const
                { return _slice->offset() + unfold(Base::_pos, _slice->size()); }
                constexpr decltype(auto) operator*() const { return (*_slice)[key()]; }

                constexpr iterator(const Slice* slice, ptrdiff_t pos)
                        : Base{pos}, _slice{slice} {}
                constexpr iterator() = default;
        };

        constexpr Slice(T& base, const key_type& offset, const key_type& size)
                : _base{base}, _offset{offset}, _size{size}
        {
                _cap = 1;
                for (auto e : size) _cap *= (size_t)e;
        }

        constexpr iterator begin() const { return iterator(this, 0); }
        constexpr iterator end() const { return iterator(this, _cap); }

        constexpr decltype(auto) operator[](const key_type& k) const { return _base[k]; }

        constexpr const key_type& offset() const { return _offset; }
        constexpr const key_type& size() const { return _size; }
        constexpr const size_t capacity() const { return _cap; }
        constexpr T& base() const { return _base; }
};

struct slice_fn {
        template<class T, vector Key>
        constexpr auto operator()(T&& t, const Key& min, const Key& max) const
        { return Slice<std::remove_reference_t<T>>(std::forward<T>(t), min, max - min); }

        template<class T, box B>
        constexpr auto operator()(T&& t, const B& b) const
        { return (*this)(std::forward<T>(t), b.min(), b.max()); }
};

inline constexpr slice_fn slice;

}; // namespace geom

#endif // GEOM_TABLE_H_
