
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)/.local
includedir := $(prefix)/include

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# Overridable flags

CXXFLAGS := -g

# Override these flags so they can't be re-overriden.

override CXXFLAGS += -std=c++20
override CXXFLAGS += -MMD
override CXXFLAGS += -fPIC
override CXXFLAGS += -Wall
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors

override CPPFLAGS += -I$(root_dir)
override CPPFLAGS += $(shell pkg-config --cflags fmt)

override LDLIBS += -lstdc++
override LDLIBS += $(shell pkg-config --libs fmt)


ifneq ($(findstring s,$(filter-out --%,$(MAKEFLAGS))),)
    QUIET_CC = @echo CC $@;
    QUIET_CXX = @echo CXX $@;
    QUIET_LINK = @echo LINK $@;
    QUIET_AR = @echo AR $@;
    QUIET_GEN = @echo GEN $@;
    descend = @echo ENTER `pwd`/$(1); make -C $(1) $(2) $(3); echo EXIT `pwd`/$(1)
endif

%: %.cc

%.o: %.c
	$(QUIET_CC)$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

%.o: %.cc
	$(QUIET_CXX)$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

%: %.o
	$(QUIET_LINK)$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

%.a:
	$(QUIET_AR)$(AR) rc $@ $^

%.s: %.cc
	$(QUIET_CXX)$(CXX) $(CPPFLAGS) $(CXXFLAGS) -S -o $@ $<

# All tests to be compiled and executed

tests += tests/unit
tests += tests/num
tests += tests/vec2
tests += tests/vec3
tests += tests/box2
tests += tests/box3
tests += tests/point
tests += tests/pool
tests += tests/cantor_hash
tests += tests/szudzik_hash
tests += tests/packed_table
tests += tests/sparse_table
tests += tests/packed_table_slice
tests += tests/sparse_table_slice

.PHONY: all
all: geom/operators.h

geom/operators.h: make_operators.sh
	$(QUIET_GEN)sh $< > $@

.PHONY: check
check: all $(addsuffix .test,$(tests))

%.test: %
	@runtest() {\
		if $$1;\
		then echo "\033[0;32mpass: \033[1;32m$$1\033[0;0m"; return 0;\
		else echo "\033[0;31mfail: \033[1;31m$$1\033[0;0m"; return 1;\
		fi;\
	};\
	runtest $<

.PHONY: clean
clean:
	$(RM) $(tests)
	$(RM) geom/operators.h
	$(RM) tests/*.o
	$(RM) tests/*.d

headers += geom/math.h
headers += geom/num.h
headers += geom/operators.h
headers += geom/pool.h
headers += geom/table.h
headers += geom/packed_table.h
headers += geom/sparse_table.h
headers += geom/tree.h

.PHONY: install
install:
	$(INSTALL) -d $(DESTDIR)$(includedir)/geom
	$(INSTALL_DATA) $(headers) $(DESTDIR)$(includedir)/geom

.PHONY: uninstall
uninstall:
	-$(RM) $(addprefix $(DESTDIR)$(includedir)/,$(headers))
	-$(RM) -d $(DESTDIR)$(includedir)/geom

-include $(shell find -name "*.d")
