#!/bin/sh

unary_assign() {
        echo "
template<number T, size_t N>
constexpr Vec<T, N>& operator$1(Vec<T, N>& t)
{
        [&t]<size_t... I>(std::index_sequence<I...>)
        { (($1 t[I]),...); }
        (std::make_index_sequence<N>{});
        return t;
}

template<number T, size_t N>
constexpr Box<T, N>& operator$1(Box<T, N>& t)
{
        [&t]<size_t... I>(std::index_sequence<I...>)
        { (($1 t[I]),...); }
        (std::make_index_sequence<N>{});
        return t;
}"
}

binary_assign () {
        echo "
template<number T, number U, size_t N>
constexpr Vec<T, N>& operator$1(Vec<T, N>& t, const U& u)
{
        [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { ((t[I] $1 u),...); }
        (std::make_index_sequence<N>{});
        return t;
}

template<number T, number U, size_t N>
constexpr Vec<T, N>& operator$1(Vec<T, N>& t, const Vec<U, N>& u)
{
        [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { ((t[I] $1 u[I]),...); }
        (std::make_index_sequence<N>{});
        return t;
}

template<number T, number U, size_t N>
constexpr Box<T, N>& operator$1(Box<T, N>& t, const U& u)
{
        [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { ((t[I] $1 u),...); }
        (std::make_index_sequence<N>{});
        return t;
}

template<number T, number U, size_t N>
constexpr Box<T, N>& operator$1(Box<T, N>& t, const Vec<U, N>& u)
{
        [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { ((t[I] $1 u[I]),...); }
        (std::make_index_sequence<N>{});
        return t;
}

template<number T, number U, size_t N>
constexpr Box<T, N>& operator$1(Box<T, N>& t, const Box<U, N>& u)
{
        [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { ((t[I] $1 u[I]),...); }
        (std::make_index_sequence<N>{});
        return t;
}"
}

unary_op() {
        echo "
template<number T, size_t N>
constexpr auto operator$1(const Vec<T, N>& t)
{
        return [&t]<size_t... I>(std::index_sequence<I...>)
        { return make_vec(($1 t[I])...); }
        (std::make_index_sequence<N>{});
}

template<number T, size_t N>
constexpr auto operator$1(const Box<T, N>& t)
{
        return [&t]<size_t... I>(std::index_sequence<I...>)
        { return make_box(($1 t[I])...); }
        (std::make_index_sequence<N>{});
}"
}

binary_op () {
        echo "
template<number T, number U, size_t N>
constexpr auto operator$1(const T& t, const Vec<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_vec((t $1 u[I])...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr auto operator$1(const T& t, const Box<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_box((t $1 u[I])...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr auto operator$1(const Vec<T, N>& t, const U& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_vec((t[I] $1 u)...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr auto operator$1(const Vec<T, N>& t, const Vec<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_vec((t[I] $1 u[I])...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr auto operator$1(const Vec<T, N>& t, const Box<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_box((t[I] $1 u[I])...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr auto operator$1(const Box<T, N>& t, const U& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_box((t[I] $1 u)...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr auto operator$1(const Box<T, N>& t, const Vec<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_box((t[I] $1 u[I])...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr auto operator$1(const Box<T, N>& t, const Box<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return make_box((t[I] $1 u[I])...); }
        (std::make_index_sequence<N>{});
}"
}

binary_assign "+="
binary_assign "-="
binary_assign "*="
binary_assign "/="
binary_assign "%="
binary_assign "&="
binary_assign "|="
binary_assign "^="
binary_assign "<<="
binary_assign ">>="

unary_assign "++"
unary_assign "--"

binary_op "+"
binary_op "-"
binary_op "*"
binary_op "/"
binary_op "%"
unary_op "+"
unary_op "-"

binary_op "&"
binary_op "|"
binary_op "^"
unary_op "~"

binary_op "&&"
binary_op "||"
unary_op "!"

binary_op "<<"
binary_op ">>"

echo "
template<number T, size_t N>
constexpr Vec<T, N> operator++(Vec<T, N>& t, int)
{ auto r = t; ++t; return r; }

template<number T, size_t N>
constexpr Box<T, N> operator++(Box<T, N>& t, int)
{ auto r = t; ++t; return r; }

template<number T, size_t N>
constexpr Vec<T, N> operator--(Vec<T, N>& t, int)
{ auto r = t; --t; return r; }

template<number T, size_t N>
constexpr Box<T, N> operator--(Box<T, N>& t, int)
{ auto r = t; --t; return r; }

template<number T, number U, size_t N>
constexpr bool operator==(const Vec<T, N>& t, const Vec<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return ((t[I] == u[I]) && ...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr bool operator==(const Box<T, N>& t, const Box<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return ((t[I] == u[I]) && ...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr bool operator!=(const Vec<T, N>& t, const Vec<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return ((t[I] != u[I]) || ...); }
        (std::make_index_sequence<N>{});
}

template<number T, number U, size_t N>
constexpr bool operator!=(const Box<T, N>& t, const Box<U, N>& u)
{
        return [&t, &u]<size_t... I>(std::index_sequence<I...>)
        { return ((t[I] != u[I]) || ...); }
        (std::make_index_sequence<N>{});
}"
