
#include <cassert>

#include "geom/math.h"

using as = geom::Num<int, geom::Unit<>, std::ratio<1>>;
using bs = geom::Num<int, geom::Unit<>, std::ratio<1, 8>>;

constexpr auto operator ""_as(unsigned long long n) { return as(n); }
constexpr auto operator ""_bs(unsigned long long n) { return bs(n); }

using distance_tag = geom::Unit<1, 0>;
using duration_tag = geom::Unit<0, 1>;
using velocity_tag = geom::Unit<1, -1>;

constexpr auto operator ""_m(unsigned long long n)
{ return geom::Num<int, distance_tag, std::ratio<1>>(n); }
constexpr auto operator ""_mm(unsigned long long n)
{ return geom::Num<int, distance_tag, std::milli>(n); }
constexpr auto operator ""_Km(unsigned long long n)
{ return geom::Num<int, distance_tag, std::kilo>(n); }

constexpr auto operator ""_s(unsigned long long n)
{ return geom::Num<int, duration_tag, std::ratio<1>>(n); }
constexpr auto operator ""_ms(unsigned long long n)
{ return geom::Num<int, duration_tag, std::milli>(n); }
constexpr auto operator ""_Ks(unsigned long long n)
{ return geom::Num<int, duration_tag, std::kilo>(n); }

constexpr auto operator ""_m_s(unsigned long long n)
{ return geom::Num<int, velocity_tag, std::ratio<1>>(n); }
constexpr auto operator ""_mm_s(unsigned long long n)
{ return geom::Num<int, velocity_tag, std::milli>(n); }
constexpr auto operator ""_Km_s(unsigned long long n)
{ return geom::Num<int, velocity_tag, std::kilo>(n); }

int main(int argc, char* argv[])
{
        {
                //assert(geom::dimension<as>{} == 1);
                //assert(geom::dimension<bs>{} == 1);
        }

        {
                auto n = 0_as;
                assert(geom::get(n) == 0);
        }

        {
                auto n = 1_as;
                assert(geom::get(n) == 1);
        }

        {
                auto n = -1_as;
                assert(geom::get(n) == -1);
        }

        {
                assert(0_as == 0_as);
                assert(!(0_as == 10_as));
        }

        {
                assert(!(0_as != 0_as));
                assert(0_as != 10_as);
        }

        {
                assert(0_as == 0_bs);
                assert(1_as == 8_bs);
                assert(8_bs == 1_as);
        }

        {
                auto n = 0_as;
                assert(++n == 1_as);
                assert(n == 1_as);
                assert(++n == 2_as);
                assert(n == 2_as);
        }

        {
                auto n = 0_as;
                assert(--n == -1_as);
                assert(n == -1_as);
                assert(--n == -2_as);
                assert(n == -2_as);
        }

        {
                auto n = 0_as;
                assert(n++ == 0_as);
                assert(n == 1_as);
                assert(n++ == 1_as);
                assert(n == 2_as);
        }

        {
                auto n = 0_as;
                assert(n-- == 0_as);
                assert(n == -1_as);
                assert(n-- == -1_as);
                assert(n == -2_as);
        }

        {
                assert(-0_as == 0_as);
                assert(-1_as == -1_as);
                assert(-(-1_as) == 1_as);
        }

        {
                auto n = 0_as;
                assert((n += 10_as) == 10_as);
                assert(n == 10_as);
                assert((n += 10_as) == 20_as);
                assert(n == 20_as);
        }

        {
                auto n = 0_as;
                assert((n -= 10_as) == -10_as);
                assert(n == -10_as);
                assert((n -= 10_as) == -20_as);
                assert(n == -20_as);
        }

        {
                as n(1);
                assert((n *= 2_as) == 2_as);
                assert(n == 2_as);
                assert((n *= 2_as) == 4_as);
                assert(n == 4_as);
        }

        {
                as n(2);
                assert((n /= 2_as) == 1_as);
                assert(n == 1_as);
                assert((n /= 2_as) == 0_as);
                assert(n == 0_as);
        }

        {
                as n(13);
                assert((n %= 7_as) == 6_as);
                assert(n == 6_as);
                assert((n %= 4_as) == 2_as);
                assert(n == 2_as);
        }

        {
                assert((0_as + 10_as) == 10_as);
                assert((10_as + 10_as) == 20_as);
        }

        {
                assert((0_as - 10_as) == -10_as);
                assert((-10_as - 10_as) == -20_as);
        }

        {
                assert((1_as * 2_as) == 2_as);
                assert((2_as * 2_as) == 4_as);
                assert(1_m_s * 1_s == 1_m);
                assert(1_mm_s * 1_s == 1_mm);
                assert(1_Km_s * 1_s == 1_Km);
                assert(1_m_s * 1000_ms == 1_m);
                assert(1_m_s * 1_Ks == 1000_m);
        }

        {
                assert((2_as / 2_as) == 1_as);
                assert((1_as / 2_as) == 0_as);
                assert(1_m / 1_s == 1_m_s);
                assert(1_mm / 1_s == 1_mm_s);
                assert(1_Km / 1_s == 1_Km_s);
                assert(1_mm / 1_ms == 1_m_s);
                assert(1_Km / 1_Ks == 1_m_s);
        }

        {
                assert((13_as % 7_as) == 6_as);
                assert((6_as % 4_as) == 2_as);
        }

        {
                assert(geom::down_cast<as>(-16_bs) == -2_as);
                assert(geom::down_cast<as>(-9_bs) == -2_as);
                assert(geom::down_cast<as>(-8_bs) == -1_as);
                assert(geom::down_cast<as>(-1_bs) == -1_as);
                assert(geom::down_cast<as>(0_bs) == 0_as);
                assert(geom::down_cast<as>(1_bs) == 0_as);
                assert(geom::down_cast<as>(7_bs) == 0_as);
                assert(geom::down_cast<as>(8_bs) == 1_as);
                assert(geom::down_cast<as>(15_bs) == 1_as);

                assert(geom::down_cast<bs>(-2_as) == -16_bs);
                assert(geom::down_cast<bs>(-1_as) == -8_bs);
                assert(geom::down_cast<bs>(0_as) == 0_bs);
                assert(geom::down_cast<bs>(1_as) == 8_bs);
                assert(geom::down_cast<bs>(2_as) == 16_bs);
        }

        {
                assert(geom::up_cast<as>(-16_bs) == -2_as);
                assert(geom::up_cast<as>(-15_bs) == -1_as);
                assert(geom::up_cast<as>(-8_bs) == -1_as);
                assert(geom::up_cast<as>(-7_bs) == 0_as);
                assert(geom::up_cast<as>(0_bs) == 0_as);
                assert(geom::up_cast<as>(1_bs) == 1_as);
                assert(geom::up_cast<as>(8_bs) == 1_as);
                assert(geom::up_cast<as>(9_bs) == 2_as);
                assert(geom::up_cast<as>(16_bs) == 2_as);

                assert(geom::up_cast<bs>(-2_as) == -16_bs);
                assert(geom::up_cast<bs>(-1_as) == -8_bs);
                assert(geom::up_cast<bs>(0_as) == 0_bs);
                assert(geom::up_cast<bs>(1_as) == 8_bs);
                assert(geom::up_cast<bs>(2_as) == 16_bs);
        }

        {
                assert(std::to_string(0_as) == "0");
                assert(std::to_string(1_as) == "1");
                assert(std::to_string(-1_as) == "-1");
                assert(std::to_string(0_bs) == "0");
                assert(std::to_string(1_bs) == "1");
                assert(std::to_string(-1_bs) == "-1");
        }

        return 0;
}
