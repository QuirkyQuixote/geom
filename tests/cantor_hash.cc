

#include <cassert>
#include <algorithm>
#include <map>

#include "geom/math.h"

#include "test.h"

Samples samples;

template<geom::vector T> struct Test {
        using value_type = geom::geom_value_t<T>;
        static constexpr size_t N = geom::geom_size_v<T>;

        static_assert(geom::vector<T>);

        void operator()() const
        {
                geom::Cantor_hash hash;
                std::map<size_t, T> m;
                for (auto p : geom::frame(T{}, samples.make<T>(3, 3)))
                        m.emplace(hash(p), p);
                assert_comparable(m.size(), pow(3, geom::geom_size_v<T>));
        }
};


int main(int argc, char* argv[])
{
        Test<geom::Vec<int, 2>>{}();
        Test<geom::Vec<int, 3>>{}();
}
