
#include <cassert>
#include <random>

#include "geom/math.h"

template<class T, class U> void assert_identical(const T& l, const U& r)
{
        static_assert(std::is_same_v<T, U>);
        assert(l == r);
}

struct Samples {
        std::random_device rd{};
        std::mt19937 gen{rd()};

        template<std::integral I> auto num()
        {
                std::uniform_int_distribution<I> distr;
                return distr(gen);
        }

        template<class T> auto vec()
        { return geom::Vec<T, 3>(num<T>(), num<T>(), num<T>()); }

        template<class T> auto box()
        { return geom::Box<T, 3>(num<T>(), num<T>(), num<T>(), num<T>(), num<T>(), num<T>()); }
};

template<class T> void test_unary(Samples& samples)
{
        // Test equality comparison between the same object

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert(l == l);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert(!(l != l));
        }

        // Test unary operators

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto s = decltype(l)(l.l + 1, l.r + 1, l.d + 1, l.u + 1, l.f + 1, l.b + 1);
                assert_identical(++l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto s = decltype(l)(l.l - 1, l.r - 1, l.d - 1, l.u - 1, l.f - 1, l.b - 1);
                assert_identical(--l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto p = l;
                auto s = decltype(l)(l.l + 1, l.r + 1, l.d + 1, l.u + 1, l.f + 1, l.b + 1);
                assert_identical(l++, p);
                assert_identical(l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto p = l;
                auto s = decltype(l)(l.l - 1, l.r - 1, l.d - 1, l.u - 1, l.f - 1, l.b - 1);
                assert_identical(l--, p);
                assert_identical(l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(+l, geom::make_box(+l.l, +l.r, +l.d, +l.u, +l.f, +l.b));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(-l, geom::make_box(-l.r, -l.l, -l.u, -l.d, -l.b, -l.f));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(~l, geom::make_box(~l.l, ~l.r, ~l.d, ~l.u, ~l.f, ~l.b));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(!l, geom::make_box(!l.l, !l.r, !l.d, !l.u, !l.f, !l.b));
        }
}

template<class T, class U> void test_binary(Samples& samples)
{
        // Test equality comparison with a different object

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert((l == r) == (l.l == r.l && l.r == r.r && l.d == r.d && l.u == r.u && l.f == r.f && l.b == r.b));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert((l != r) == (l.l != r.l || l.r != r.r || l.d != r.d || l.u == r.u || l.f != r.b || l.b != r.b));
        }

        // Test compound assigment with num

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l + r, l.r + r, l.d + r, l.u + r, l.f + r, l.b + r);
                assert_identical(l += r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l - r, l.r - r, l.d - r, l.u - r, l.f - r, l.b - r);
                assert_identical(l -= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l * r, l.r * r, l.d * r, l.u * r, l.f * r, l.b * r);
                assert_identical(l *= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                auto m = decltype(l)(l.l / r, l.r / r, l.d / r, l.u / r, l.f / r, l.b / r);
                assert_identical(l /= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                auto m = decltype(l)(l.l % r, l.r % r, l.d % r, l.u % r, l.f % r, l.b % r);
                assert_identical(l %= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l | r, l.r | r, l.d | r, l.u | r, l.f | r, l.b | r);
                assert_identical(l |= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l & r, l.r & r, l.d & r, l.u & r, l.f & r, l.b & r);
                assert_identical(l &= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l ^ r, l.r ^ r, l.d ^ r, l.u ^ r, l.f ^ r, l.b ^ r);
                assert_identical(l ^= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l << r, l.r << r, l.d << r, l.u << r, l.f << r, l.b << r);
                assert_identical(l <<= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l >> r, l.r >> r, l.d >> r, l.u >> r, l.f >> r, l.b >> r);
                assert_identical(l >>= r, m);
        }

        // Test compound assignment with vector

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l + r.x, l.r + r.x, l.d + r.y, l.u + r.y, l.f + r.z, l.b + r.z);
                assert_identical(l += r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l - r.x, l.r - r.x, l.d - r.y, l.u - r.y, l.f - r.z, l.b - r.z);
                assert_identical(l -= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l * r.x, l.r * r.x, l.d * r.y, l.u * r.y, l.f * r.z, l.b * r.z);
                assert_identical(l *= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                auto m = decltype(l)(l.l / r.x, l.r / r.x, l.d / r.y, l.u / r.y, l.f / r.z, l.b / r.z);
                assert_identical(l /= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                auto m = decltype(l)(l.l % r.x, l.r % r.x, l.d % r.y, l.u % r.y, l.f % r.z, l.b % r.z);
                assert_identical(l %= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l | r.x, l.r | r.x, l.d | r.y, l.u | r.y, l.f | r.z, l.b | r.z);
                assert_identical(l |= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l & r.x, l.r & r.x, l.d & r.y, l.u & r.y, l.f & r.z, l.b & r.z);
                assert_identical(l &= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l ^ r.x, l.r ^ r.x, l.d ^ r.y, l.u ^ r.y, l.f ^ r.z, l.b ^ r.z);
                assert_identical(l ^= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l << r.x, l.r << r.x, l.d << r.y, l.u << r.y, l.f << r.z, l.b << r.z);
                assert_identical(l <<= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l >> r.x, l.r >> r.x, l.d >> r.y, l.u >> r.y, l.f >> r.z, l.b >> r.z);
                assert_identical(l >>= r, m);
        }

        // Test binary operation with num

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l + r, geom::make_box(l.l + r, l.r + r, l.d + r, l.u + r, l.f + r, l.b + r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l - r, geom::make_box(l.l - r, l.r - r, l.d - r, l.u - r, l.f - r, l.b - r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l * r, geom::make_box(l.l * r, l.r * r, l.d * r, l.u * r, l.f * r, l.b * r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                assert_identical(l / r, geom::make_box(l.l / r, l.r / r, l.d / r, l.u / r, l.f / r, l.b / r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                assert_identical(l % r, geom::make_box(l.l % r, l.r % r, l.d % r, l.u % r, l.f % r, l.b % r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l | r, geom::make_box(l.l | r, l.r | r, l.d | r, l.u | r, l.f | r, l.b | r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l & r, geom::make_box(l.l & r, l.r & r, l.d & r, l.u & r, l.f & r, l.b & r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l ^ r, geom::make_box(l.l ^ r, l.r ^ r, l.d ^ r, l.u ^ r, l.f ^ r, l.b ^ r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l << r, geom::make_box(l.l << r, l.r << r, l.d << r, l.u << r, l.f << r, l.b << r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l >> r, geom::make_box(l.l >> r, l.r >> r, l.d >> r, l.u >> r, l.f >> r, l.b >> r));
        }

        // Test binary operation with vector

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l + r, geom::make_box(l.l + r.x, l.r + r.x, l.d + r.y, l.u + r.y, l.f + r.z, l.b + r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l - r, geom::make_box(l.l - r.x, l.r - r.x, l.d - r.y, l.u - r.y, l.f - r.z, l.b - r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l * r, geom::make_box(l.l * r.x, l.r * r.x, l.d * r.y, l.u * r.y, l.f * r.z, l.b * r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                assert_identical(l / r, geom::make_box(l.l / r.x, l.r / r.x, l.d / r.y, l.u / r.y, l.f / r.z, l.b / r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                assert_identical(l % r, geom::make_box(l.l % r.x, l.r % r.x, l.d % r.y, l.u % r.y, l.f % r.z, l.b % r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l | r, geom::make_box(l.l | r.x, l.r | r.x, l.d | r.y, l.u | r.y, l.f | r.z, l.b | r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l & r, geom::make_box(l.l & r.x, l.r & r.x, l.d & r.y, l.u & r.y, l.f & r.z, l.b & r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l ^ r, geom::make_box(l.l ^ r.x, l.r ^ r.x, l.d ^ r.y, l.u ^ r.y, l.f ^ r.z, l.b ^ r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l << r, geom::make_box(l.l << r.x, l.r << r.x, l.d << r.y, l.u << r.y, l.f << r.z, l.b << r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l >> r, geom::make_box(l.l >> r.x, l.r >> r.x, l.d >> r.y, l.u >> r.y, l.f >> r.z, l.b >> r.z));
        }

        // Contains and intersects

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(geom::contains(l, r), l.l <= r.x && l.r >= r.x && l.d <= r.y && l.u >= r.y && l.f <= r.z && l.b >= r.z);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert_identical(geom::contains(l, r), l.l <= r.l && l.r >= r.r && l.d <= r.d && l.u >= r.u && l.f <= r.f && l.b >= r.b);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert_identical(geom::intersects(l, r), l.l <= r.r && l.r >= r.l && l.d <= r.u && l.u >= r.d && l.f <= r.b && l.b >= r.f);
        }

}

int main(int argc, char* argv[])
{
        Samples samples;

        {
                geom::Box<int, 3> v;
                assert(v.l == 0);
                assert(v.r == 0);
                assert(v.d == 0);
                assert(v.u == 0);
                assert(v.f == 0);
                assert(v.b == 0);
        }

        // Test iterator

        {
                geom::Box<int, 3> v;
                assert(std::addressof(v.l) == std::addressof(geom::begin(v)[0].min));
                assert(std::addressof(v.r) == std::addressof(geom::begin(v)[0].max));
                assert(std::addressof(v.d) == std::addressof(geom::begin(v)[1].min));
                assert(std::addressof(v.u) == std::addressof(geom::begin(v)[1].max));
                assert(std::addressof(v.f) == std::addressof(geom::begin(v)[2].min));
                assert(std::addressof(v.b) == std::addressof(geom::begin(v)[2].max));
        }

        // Test element access

        {
                geom::Box<int, 3> v;
                assert(std::addressof(v.l) == std::addressof(v[0].min));
                assert(std::addressof(v.r) == std::addressof(v[0].max));
                assert(std::addressof(v.d) == std::addressof(v[1].min));
                assert(std::addressof(v.u) == std::addressof(v[1].max));
                assert(std::addressof(v.f) == std::addressof(v[2].min));
                assert(std::addressof(v.b) == std::addressof(v[2].max));
        }

        test_unary<char>(samples);
        test_unary<int>(samples);
        test_unary<long long>(samples);

        test_binary<char, char>(samples);
        test_binary<char, int>(samples);
        test_binary<char, long long>(samples);
        test_binary<int, char>(samples);
        test_binary<int, int>(samples);
        test_binary<int, long long>(samples);
        test_binary<long long, char>(samples);
        test_binary<long long, int>(samples);
        test_binary<long long, long long>(samples);

        return 0;
}

