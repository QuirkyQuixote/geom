
#include <cassert>

#include "geom/math.h"

#include "test.h"

Samples samples;

template<geom::box T> struct Test {
        void center() const
        {
                T t = samples.make<T>(0, 10);
                auto c = geom::center(t);
                assert(c.x == (t.l + t.r) / 2);
                assert(c.y == (t.d + t.u) / 2);
        }

        void operator()() const
        {
                center();
        }
};

int main(int argc, char* argv[])
{
        Test<geom::Box<int, 2>>{}();
}
