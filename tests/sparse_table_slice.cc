

#include <cassert>
#include <algorithm>

#include "geom/sparse_table.h"

#include "test.h"

Samples samples;

template<geom::table T> struct Test {
        using key_type = geom::table_key_t<T>;
        using mapped_type = geom::table_mapped_t<T>;

        static_assert(geom::vector<key_type>);

        static_assert(std::input_or_output_iterator<typename T::iterator>);
        static_assert(std::input_iterator<typename T::iterator>);
        static_assert(std::forward_iterator<typename T::iterator>);
        static_assert(std::bidirectional_iterator<typename T::iterator>);
        static_assert(std::random_access_iterator<typename T::iterator>);

        static_assert(std::ranges::range<T>);
        static_assert(std::ranges::input_range<T>);
        static_assert(std::ranges::forward_range<T>);
        static_assert(std::ranges::bidirectional_range<T>);
        static_assert(std::ranges::random_access_range<T>);

        static_assert(geom::table<T>);

        void _iterator_distance() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto a = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                auto b = samples.make<key_type>(0, 10);
                T t(o, a + s + b);
                auto u = geom::slice(t, o + a, o + a + s);
                assert_comparable(std::distance(u.begin(), u.end()),
                                std::accumulate(std::ranges::begin(s),
                                        std::ranges::end(s),
                                        1,
                                        std::multiplies{}));
        }

        void _iterator_path() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto a = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                auto b = samples.make<key_type>(0, 10);
                T t(o, a + s + b);
                auto u = geom::slice(t, o + a, o + a + s);
                auto j = u.begin();
                for (auto p : geom::frame(key_type{}, s))
                        assert_comparable(geom::path(u.begin(), j++), p);
        }

        void _iterator_value_1() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto a = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                auto b = samples.make<key_type>(0, 10);
                T t(o, a + s + b);
                auto u = geom::slice(t, o + a, o + a + s);
                std::vector<mapped_type> v(u.capacity());
                std::ranges::generate(v, [](){ return samples.make<mapped_type>(); });
                std::ranges::copy(v, u.begin());
                auto j = v.begin();
                for (auto p : geom::frame(o + a, o + a + s))
                        assert_comparable(*j++, u[p]);
        }

        void _iterator_value_2() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto a = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                auto b = samples.make<key_type>(0, 10);
                T t(o, a + s + b);
                auto u = geom::slice(t, o + a, o + a + s);
                std::vector<mapped_type> v(u.capacity());
                std::ranges::generate(u, [](){ return samples.make<mapped_type>(); });
                std::ranges::copy(u, v.begin());
                auto j = v.begin();
                for (auto p : geom::frame(o + a, o + a + s))
                        assert_comparable(*j++, u[p]);
        }

        void operator()() const
        {
                for (size_t n = 0; n != 1000; ++n) {
                        _iterator_distance();
                        _iterator_path();
                        _iterator_value_1();
                        _iterator_value_2();
                }
        }
};


int main(int argc, char* argv[])
{
        Test<geom::Sparse_table<geom::Vec<char, 2>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<int, 2>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<long, 2>, int>>{}();

        Test<geom::Sparse_table<geom::Vec<char, 3>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<int, 3>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<long, 3>, int>>{}();
}
