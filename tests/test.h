
#ifndef GEOM_TEST_TEST_H_
#define GEOM_TEST_TEST_H_

#include <iostream>
#include <string>
#include <random>
#include <type_traits>

#include "geom/math.h"

template<class T, class U> void assert_comparable(const T& l, const U& r)
{
        assert(l == r);
}

template<class T, class U> void assert_identical(const T& l, const U& r)
{
        static_assert(std::is_same_v<T, U>);
        assert_comparable(l, r);
}

struct Samples {
        std::random_device rd{};
        std::mt19937 gen{rd()};

        template<geom::number T>
        T make(int a = std::numeric_limits<geom::num_rep_t<T>>::min(),
                        int b = std::numeric_limits<geom::num_rep_t<T>>::max())
        {
                std::uniform_int_distribution<decltype(geom::get(std::declval<T>()))> distr(a, b);
                return T{distr(gen)};
        }

        template<geom::vector T>
        T make(int a = std::numeric_limits<geom::num_rep_t<T>>::min(),
                        int b = std::numeric_limits<geom::num_rep_t<T>>::max())
        {
                using U = geom::geom_value_t<T>;
                if constexpr (geom::geom_size_v<T> == 1)
                        return T{make<U>(a, b)};
                if constexpr (geom::geom_size_v<T> == 2)
                        return T{make<U>(a, b), make<U>(a, b)};
                if constexpr (geom::geom_size_v<T> == 3)
                        return T{make<U>(a, b), make<U>(a, b), make<U>(a, b)};
                if constexpr (geom::geom_size_v<T> == 4)
                        return T{make<U>(a, b), make<U>(a, b), make<U>(a, b), make<U>(a, b)};
                throw std::runtime_error{"unimplemented"};
        }

        template<geom::box T>
        T make(int a = std::numeric_limits<geom::num_rep_t<T>>::min(),
                        int b = std::numeric_limits<geom::num_rep_t<T>>::max())
        {
                using U = std::decay_t<decltype(std::declval<T>().min())>;
                auto o = make<U>(a, b);
                auto s = make<U>(a, b);
                return T(o, o + s);
        }
};

#endif // GEOM_TEST_TEST_H_
