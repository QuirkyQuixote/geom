
#include <cassert>

#include "geom/math.h"

#include "test.h"

Samples samples;

template<geom::vector T> struct Test {
        using value_type = geom::geom_value_t<T>;

        void _constructor_0() const
        {
                T t;
                assert(t.x == value_type{});
                assert(t.y == value_type{});
        }

        template<class X, class Y> void _constructor_1() const
        {
                X x = samples.make<X>();
                Y y = samples.make<Y>();
                T t{x, y};
                assert((X)t.x == x);
                assert((Y)t.y == y);
        }

        template<class U> void _constructor_2() const
        {
                U u = samples.make<U>();
                T t{u};
                assert((geom::geom_value_t<U>)t.x == u.x);
                assert((geom::geom_value_t<U>)t.y == u.y);
        }

        template<class U> void _assignment() const
        {
                U u = samples.make<U>();
                T t;
                t = u;
                assert((geom::geom_value_t<U>)t.x == u.x);
                assert((geom::geom_value_t<U>)t.y == u.y);
        }

        void operator()() const
        {
                _constructor_0();
                _constructor_1<int, int>();
                _constructor_1<int, char>();
                _constructor_1<int, unsigned>();
                _constructor_1<char, int>();
                _constructor_1<char, char>();
                _constructor_1<char, unsigned>();
                _constructor_1<unsigned, int>();
                _constructor_1<unsigned, char>();
                _constructor_1<unsigned, unsigned>();
                _constructor_2<geom::Vec<int, 2>>();
                _constructor_2<geom::Vec<char, 2>>();
                _constructor_2<geom::Vec<unsigned, 2>>();
                _assignment<geom::Vec<int, 2>>();
                _assignment<geom::Vec<char, 2>>();
                _assignment<geom::Vec<unsigned, 2>>();
        }
};

int main(int argc, char* argv[])
{
        Test<geom::Vec<int, 2>>{}();
        Test<geom::Vec<long, 2>>{}();
        Test<geom::Vec<long long, 2>>{}();
        Test<geom::Vec<unsigned, 2>>{}();
        Test<geom::Vec<unsigned long, 2>>{}();
        Test<geom::Vec<unsigned long long, 2>>{}();

        return 0;
}

