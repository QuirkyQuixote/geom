
#include <cassert>
#include <random>

#include "geom/math.h"

template<class T, class U> void assert_identical(const T& l, const U& r)
{
        static_assert(std::is_same_v<T, U>);
        assert(l == r);
}

struct Samples {
        std::random_device rd{};
        std::mt19937 gen{rd()};

        template<std::integral I> auto num()
        {
                std::uniform_int_distribution<I> distr;
                return distr(gen);
        }

        template<class T> auto vec()
        { return geom::Vec<T, 2>(num<T>(), num<T>()); }

        template<class T> auto box()
        { return geom::Box<T, 2>(num<T>(), num<T>(), num<T>(), num<T>()); }
};

template<class T> void test_unary(Samples& samples)
{
        // Test equality comparison between the same object

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert(l == l);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert(!(l != l));
        }

        // Test unary operators

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto s = decltype(l)(l.l + 1, l.r + 1, l.d + 1, l.u + 1);
                assert_identical(++l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto s = decltype(l)(l.l - 1, l.r - 1, l.d - 1, l.u - 1);
                assert_identical(--l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto p = l;
                auto s = decltype(l)(l.l + 1, l.r + 1, l.d + 1, l.u + 1);
                assert_identical(l++, p);
                assert_identical(l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto p = l;
                auto s = decltype(l)(l.l - 1, l.r - 1, l.d - 1, l.u - 1);
                assert_identical(l--, p);
                assert_identical(l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(+l, geom::make_box(+l.l, +l.r, +l.d, +l.u));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(-l, geom::make_box(-l.r, -l.l, -l.u, -l.d));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(~l, geom::make_box(~l.l, ~l.r, ~l.d, ~l.u));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                assert_identical(!l, geom::make_box(!l.l, !l.r, !l.d, !l.u));
        }
}

template<class T, class U> void test_binary(Samples& samples)
{
        // Test equality comparison with a different object

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert((l == r) == (l.l == r.l && l.r == r.r && l.d == r.d && l.u == r.u));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert((l != r) == (l.l != r.l || l.r != r.r || l.d != r.d || l.u == r.u));
        }

        // Test compound assigment with num

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l + r, l.r + r, l.d + r, l.u + r);
                assert_identical(l += r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l - r, l.r - r, l.d - r, l.u - r);
                assert_identical(l -= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l * r, l.r * r, l.d * r, l.u * r);
                assert_identical(l *= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                auto m = decltype(l)(l.l / r, l.r / r, l.d / r, l.u / r);
                assert_identical(l /= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                auto m = decltype(l)(l.l % r, l.r % r, l.d % r, l.u % r);
                assert_identical(l %= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l | r, l.r | r, l.d | r, l.u | r);
                assert_identical(l |= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l & r, l.r & r, l.d & r, l.u & r);
                assert_identical(l &= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l ^ r, l.r ^ r, l.d ^ r, l.u ^ r);
                assert_identical(l ^= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l << r, l.r << r, l.d << r, l.u << r);
                assert_identical(l <<= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.l >> r, l.r >> r, l.d >> r, l.u >> r);
                assert_identical(l >>= r, m);
        }

        // Test compound assignment with vector

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l + r.x, l.r + r.x, l.d + r.y, l.u + r.y);
                assert_identical(l += r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l - r.x, l.r - r.x, l.d - r.y, l.u - r.y);
                assert_identical(l -= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l * r.x, l.r * r.x, l.d * r.y, l.u * r.y);
                assert_identical(l *= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                auto m = decltype(l)(l.l / r.x, l.r / r.x, l.d / r.y, l.u / r.y);
                assert_identical(l /= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                auto m = decltype(l)(l.l % r.x, l.r % r.x, l.d % r.y, l.u % r.y);
                assert_identical(l %= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l | r.x, l.r | r.x, l.d | r.y, l.u | r.y);
                assert_identical(l |= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l & r.x, l.r & r.x, l.d & r.y, l.u & r.y);
                assert_identical(l &= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l ^ r.x, l.r ^ r.x, l.d ^ r.y, l.u ^ r.y);
                assert_identical(l ^= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l << r.x, l.r << r.x, l.d << r.y, l.u << r.y);
                assert_identical(l <<= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.l >> r.x, l.r >> r.x, l.d >> r.y, l.u >> r.y);
                assert_identical(l >>= r, m);
        }

        // Test binary operation with num

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l + r, geom::make_box(l.l + r, l.r + r, l.d + r, l.u + r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l - r, geom::make_box(l.l - r, l.r - r, l.d - r, l.u - r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l * r, geom::make_box(l.l * r, l.r * r, l.d * r, l.u * r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                assert_identical(l / r, geom::make_box(l.l / r, l.r / r, l.d / r, l.u / r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                assert_identical(l % r, geom::make_box(l.l % r, l.r % r, l.d % r, l.u % r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l | r, geom::make_box(l.l | r, l.r | r, l.d | r, l.u | r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l & r, geom::make_box(l.l & r, l.r & r, l.d & r, l.u & r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l ^ r, geom::make_box(l.l ^ r, l.r ^ r, l.d ^ r, l.u ^ r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l << r, geom::make_box(l.l << r, l.r << r, l.d << r, l.u << r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.num<U>();
                assert_identical(l >> r, geom::make_box(l.l >> r, l.r >> r, l.d >> r, l.u >> r));
        }

        // Test binary operation with vector

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l + r, geom::make_box(l.l + r.x, l.r + r.x, l.d + r.y, l.u + r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l - r, geom::make_box(l.l - r.x, l.r - r.x, l.d - r.y, l.u - r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l * r, geom::make_box(l.l * r.x, l.r * r.x, l.d * r.y, l.u * r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                assert_identical(l / r, geom::make_box(l.l / r.x, l.r / r.x, l.d / r.y, l.u / r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                assert_identical(l % r, geom::make_box(l.l % r.x, l.r % r.x, l.d % r.y, l.u % r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l | r, geom::make_box(l.l | r.x, l.r | r.x, l.d | r.y, l.u | r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l & r, geom::make_box(l.l & r.x, l.r & r.x, l.d & r.y, l.u & r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l ^ r, geom::make_box(l.l ^ r.x, l.r ^ r.x, l.d ^ r.y, l.u ^ r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l << r, geom::make_box(l.l << r.x, l.r << r.x, l.d << r.y, l.u << r.y));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(l >> r, geom::make_box(l.l >> r.x, l.r >> r.x, l.d >> r.y, l.u >> r.y));
        }

        // Contains and intersects

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.vec<U>();
                assert_identical(geom::contains(l, r), l.l <= r.x && l.r >= r.x && l.d <= r.y && l.u >= r.y);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert_identical(geom::contains(l, r), l.l <= r.l && l.r >= r.r && l.d <= r.d && l.u >= r.u);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.box<T>();
                auto r = samples.box<U>();
                assert_identical(geom::intersects(l, r), l.l <= r.r && l.r >= r.l && l.d <= r.u && l.u >= r.d);
        }

}

int main(int argc, char* argv[])
{
        Samples samples;

        static_assert(geom::interval<geom::Interval<int>>);

        static_assert(std::ranges::contiguous_range<geom::Box<int, 2>>);
        static_assert(geom::interval<std::ranges::range_value_t<geom::Box<int, 2>>>);
        static_assert(geom::box<geom::Box<int, 2>>);

        {
                geom::Box<int, 2> v;
                assert(v.l == 0);
                assert(v.r == 0);
                assert(v.d == 0);
                assert(v.u == 0);
        }

        // Test iterator

        {
                geom::Box<int, 2> v;
                assert(std::addressof(v.l) == std::addressof(geom::begin(v)[0].min));
                assert(std::addressof(v.r) == std::addressof(geom::begin(v)[0].max));
                assert(std::addressof(v.d) == std::addressof(geom::begin(v)[1].min));
                assert(std::addressof(v.u) == std::addressof(geom::begin(v)[1].max));
        }

        // Test element access

        {
                geom::Box<int, 2> v;
                assert(std::addressof(v.l) == std::addressof(v[0].min));
                assert(std::addressof(v.r) == std::addressof(v[0].max));
                assert(std::addressof(v.d) == std::addressof(v[1].min));
                assert(std::addressof(v.u) == std::addressof(v[1].max));
        }

        test_unary<char>(samples);
        test_unary<int>(samples);
        test_unary<long long>(samples);

        test_binary<char, char>(samples);
        test_binary<char, int>(samples);
        test_binary<char, long long>(samples);
        test_binary<int, char>(samples);
        test_binary<int, int>(samples);
        test_binary<int, long long>(samples);
        test_binary<long long, char>(samples);
        test_binary<long long, int>(samples);
        test_binary<long long, long long>(samples);

        return 0;
}

