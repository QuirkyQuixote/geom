
#include <cassert>
#include <ostream>

#include "geom/num.h"

template<class Rep = int, class Ratio = std::ratio<1>> using number = geom::Num<Rep, geom::Unit<0, 0>, Ratio>;
template<class Rep = int, class Ratio = std::ratio<1>> using distance = geom::Num<Rep, geom::Unit<1, 0>, Ratio>;
template<class Rep = int, class Ratio = std::ratio<1>> using area = geom::Num<Rep, geom::Unit<2, 0>, Ratio>;
template<class Rep = int, class Ratio = std::ratio<1>> using volume = geom::Num<Rep, geom::Unit<3, 0>, Ratio>;
template<class Rep = int, class Ratio = std::ratio<1>> using duration = geom::Num<Rep, geom::Unit<0, 1>, Ratio>;
template<class Rep = int, class Ratio = std::ratio<1>> using frequency = geom::Num<Rep, geom::Unit<0, -1>, Ratio>;
template<class Rep = int, class Ratio = std::ratio<1>> using velocity = geom::Num<Rep, geom::Unit<1, -1>, Ratio>;
template<class Rep = int, class Ratio = std::ratio<1>> using acceleration = geom::Num<Rep, geom::Unit<1, -2>, Ratio>;

constexpr auto operator ""_(unsigned long long n) { return number<>(n); }
constexpr auto operator ""_m(unsigned long long n) { return distance<>(n); }
constexpr auto operator ""_m2(unsigned long long n) { return area<>(n); }
constexpr auto operator ""_m3(unsigned long long n) { return volume<>(n); }
constexpr auto operator ""_s(unsigned long long n) { return duration<>(n); }
constexpr auto operator ""_Hz(unsigned long long n) { return frequency<>(n); }
constexpr auto operator ""_m_s(unsigned long long n) { return velocity<>(n); }
constexpr auto operator ""_m_ss(unsigned long long n) { return acceleration<>(n); }

std::ostream& operator<<(std::ostream& stream, geom::Unit<0, 0>) { return stream; }
std::ostream& operator<<(std::ostream& stream, geom::Unit<1, 0>) { return stream << "m"; }
std::ostream& operator<<(std::ostream& stream, geom::Unit<2, 0>) { return stream << "m^2"; }
std::ostream& operator<<(std::ostream& stream, geom::Unit<0, 1>) { return stream << "s"; }
std::ostream& operator<<(std::ostream& stream, geom::Unit<0, 2>) { return stream << "s^2"; }
std::ostream& operator<<(std::ostream& stream, geom::Unit<0, -1>) { return stream << "Hz"; }
std::ostream& operator<<(std::ostream& stream, geom::Unit<1, -1>) { return stream << "m/s"; }
std::ostream& operator<<(std::ostream& stream, geom::Unit<1, -2>) { return stream << "m/s^2"; }

std::string to_string(geom::Unit<0, 0>) { return ""; }
std::string to_string(geom::Unit<1, 0>) { return "m"; }
std::string to_string(geom::Unit<2, 0>) { return "m^2"; }
std::string to_string(geom::Unit<0, 1>) { return "s"; }
std::string to_string(geom::Unit<0, 2>) { return "s^2"; }
std::string to_string(geom::Unit<0, -1>) { return "Hz"; }
std::string to_string(geom::Unit<1, -1>) { return "m/s"; }
std::string to_string(geom::Unit<1, -2>) { return "m/s^2"; }

int main(int argc, char* argv[])
{
        {
                assert(0_m + 0_m == 0_m);
                assert(1_m + 1_m == 2_m);
        }

        {
                assert(0_m - 0_m == 0_m);
                assert(1_m - 1_m == 0_m);
        }

        {
                assert(0_m * 0_m == 0_m2);
                assert(1_m * 1_m == 1_m2);
        }

        {
                assert(0_m / 1_m == 0_);
                assert(4_m / 2_m == 2_);
                assert(10_m / 2_s == 5_m_s);
                assert(20_m / 2_s / 2_s == 5_m_ss);
        }

        return 0;
}
