

#include <cassert>

#include "geom/pool.h"


int main()
{
        geom::Pool<std::string> pool;
        std::vector<size_t> handles;
        handles.push_back(pool.insert("0"));
        handles.push_back(pool.insert("1"));
        handles.push_back(pool.insert("2"));
        handles.push_back(pool.insert("3"));
        handles.push_back(pool.insert("4"));
        handles.push_back(pool.insert("5"));
        handles.push_back(pool.insert("6"));
        handles.push_back(pool.insert("7"));
        handles.push_back(pool.insert("8"));
        handles.push_back(pool.insert("9"));

        for (size_t i = 0; i < 10; ++i)
                assert(std::to_string(handles[i]) == pool[handles[i]]);

        for (auto x : handles)
                pool.erase(x);
}
