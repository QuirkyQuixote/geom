
#include <cassert>

#include "geom/packed_table.h"

using intv = geom::Vec<int, 3>;
using intb = geom::Box<int, 3>;
using int_table = geom::Packed_table<intv, int>;
using int_slice = geom::Slice<int_table>;

int main(int argc, char* argv[])
{
        {
                static_assert(std::input_or_output_iterator<int_slice::iterator>);
                static_assert(std::input_iterator<int_slice::iterator>);
                static_assert(std::forward_iterator<int_slice::iterator>);
                static_assert(std::bidirectional_iterator<int_slice::iterator>);

                static_assert(std::ranges::range<int_slice>);
                static_assert(std::ranges::input_range<int_slice>);
                static_assert(std::ranges::forward_range<int_slice>);
                static_assert(std::ranges::bidirectional_range<int_slice>);
                static_assert(geom::table<int_slice>);
        }

        {
                int_table t(intv{0, 0, 0}, intv{3, 3, 3});
                t = {0, 1, 2, 3, 4, 5, 6, 7, 8,
                        9, 10, 11, 12, 13, 14, 15, 16, 17,
                        18, 19, 20, 21, 22, 23, 24, 25, 26};

                {
                        auto s = geom::slice(t, intb{0, 2, 0, 2, 0, 2});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 0);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 1);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 3);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 4);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 9);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 10);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 12);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 13);
                        assert(++it == end);
                }

                {
                        auto s = geom::slice(t, intb{0, 2, 0, 2, 1, 3});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 9);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 10);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 12);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 13);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 18);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 19);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 21);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 22);
                        assert(++it == end);
                }

                {
                        auto s = geom::slice(t, intb{1, 3, 0, 2, 0, 2});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 1);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 2);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 4);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 5);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 10);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 11);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 13);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 14);
                        assert(++it == end);
                }

                {
                        auto s = geom::slice(t, intb{1, 3, 0, 2, 1, 3});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 10);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 11);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 13);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 14);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 19);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 20);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 22);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 23);
                        assert(++it == end);
                }

                {
                        auto s = geom::slice(t, intb{0, 2, 1, 3, 0, 2});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 3);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 4);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 6);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 7);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 12);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 13);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 15);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 16);
                        assert(++it == end);
                }

                {
                        auto s = geom::slice(t, intb{0, 2, 1, 3, 1, 3});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 12);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 13);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 15);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 16);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 21);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 22);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 24);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 25);
                        assert(++it == end);
                }

                {
                        auto s = geom::slice(t, intb{1, 3, 1, 3, 0, 2});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 4);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 5);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 7);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 8);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 13);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 14);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 16);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 17);
                        assert(++it == end);
                }

                {
                        auto s = geom::slice(t, intb{1, 3, 1, 3, 1, 3});
                        auto begin = s.begin();
                        auto end = s.end();
                        auto it = begin;
                        assert((geom::path(begin, it) == intv{0, 0, 0}));
                        assert(*it == 13);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 0}));
                        assert(*it == 14);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 0}));
                        assert(*it == 16);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 0}));
                        assert(*it == 17);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 0, 1}));
                        assert(*it == 22);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 0, 1}));
                        assert(*it == 23);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{0, 1, 1}));
                        assert(*it == 25);
                        assert(++it != end);
                        assert((geom::path(begin, it) == intv{1, 1, 1}));
                        assert(*it == 26);
                        assert(++it == end);
                }
        }

        return 0;
}
