

#include <cassert>
#include <algorithm>

#include "geom/packed_table.h"

#include "test.h"

Samples samples;

template<geom::table T> struct Test {
        using key_type = geom::table_key_t<T>;
        using mapped_type = geom::table_mapped_t<T>;

        static_assert(geom::vector<key_type>);

        static_assert(std::input_or_output_iterator<typename T::iterator>);
        static_assert(std::input_iterator<typename T::iterator>);
        static_assert(std::forward_iterator<typename T::iterator>);
        static_assert(std::bidirectional_iterator<typename T::iterator>);
        static_assert(std::random_access_iterator<typename T::iterator>);

        static_assert(std::ranges::range<T>);
        static_assert(std::ranges::input_range<T>);
        static_assert(std::ranges::forward_range<T>);
        static_assert(std::ranges::bidirectional_range<T>);
        static_assert(std::ranges::random_access_range<T>);

        static_assert(geom::table<T>);

        void _default_constructor() const
        {
                T t;
        }

        void _constructor_1() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                assert_comparable(t.offset(), o);
                assert_comparable(t.size(), s);
        }

        void _constructor_2() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                auto x = samples.make<mapped_type>(1, 10);
                T t(o, s, x);
                assert_comparable(t.offset(), o);
                assert_comparable(t.size(), s);
        }

        void _copy_constructor() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u(t);
        }

        void _move_constructor() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u(std::move(t));
        }

        void _copy_assignment() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u;
                u = t;
        }

        void _move_assignment() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u;
                u = std::move(t);
        }

        void _list_assignment() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                t = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        }

        void _iterator_distance() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                assert_comparable(std::distance(t.begin(), t.end()),
                                std::accumulate(std::ranges::begin(s),
                                        std::ranges::end(s),
                                        1,
                                        std::multiplies{}));
        }

        void _iterator_path() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                auto j = t.begin();
                for (auto p : geom::frame(key_type{}, s))
                        assert_comparable(geom::path(t.begin(), j++), p);
        }

        void _iterator_value_1() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                std::vector<mapped_type> v(t.capacity());
                std::ranges::generate(v, [](){ return samples.make<mapped_type>(); });
                std::ranges::copy(v, t.begin());
                auto j = v.begin();
                for (auto p : geom::frame(o, o + s))
                        assert_comparable(*j++, t[p]);
        }

        void _iterator_value_2() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                std::vector<mapped_type> v(t.capacity());
                std::ranges::generate(t, [](){ return samples.make<mapped_type>(); });
                std::ranges::copy(t, v.begin());
                auto j = v.begin();
                for (auto p : geom::frame(o, o + s))
                        assert_comparable(*j++, t[p]);
        }

        void operator()() const
        {
                for (size_t n = 0; n != 1000; ++n) {
                        _default_constructor();
                        _constructor_1();
                        _constructor_2();
                        _copy_constructor();
                        _move_constructor();
                        _copy_assignment();
                        _move_assignment();
                        _list_assignment();
                        _iterator_distance();
                        _iterator_path();
                        _iterator_value_1();
                        _iterator_value_2();
                }
        }
};


int main(int argc, char* argv[])
{
        Test<geom::Packed_table<geom::Vec<char, 2>, int>>{}();
        Test<geom::Packed_table<geom::Vec<int, 2>, int>>{}();
        Test<geom::Packed_table<geom::Vec<long, 2>, int>>{}();

        Test<geom::Packed_table<geom::Vec<char, 3>, int>>{}();
        Test<geom::Packed_table<geom::Vec<int, 3>, int>>{}();
        Test<geom::Packed_table<geom::Vec<long, 3>, int>>{}();
}
