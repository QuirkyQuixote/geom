

#include <cassert>
#include <algorithm>
#include <numeric>

#include "geom/sparse_table.h"
#include "fmt/core.h"

#include "test.h"

Samples samples;

#define TEST(x) do { if (!(x)) throw std::runtime_error{fmt::format("fail: {}: {}", __func__, #x )}; } while (0)

template<std::ranges::input_range R>
void print_range(std::ostream& os, R&& r)
{
        for (auto x : r)
                std::cout << ' ' << x;
        std::cout << '\n';
}

template<geom::table T> struct Test {
        using key_type = geom::table_key_t<T>;
        using mapped_type = geom::table_mapped_t<T>;

        static_assert(geom::vector<key_type>);

        static_assert(std::input_or_output_iterator<typename T::iterator>);
        static_assert(std::input_iterator<typename T::iterator>);
        static_assert(std::forward_iterator<typename T::iterator>);
        static_assert(std::bidirectional_iterator<typename T::iterator>);
        static_assert(std::random_access_iterator<typename T::iterator>);

        static_assert(std::ranges::range<T>);
        static_assert(std::ranges::input_range<T>);
        static_assert(std::ranges::forward_range<T>);
        static_assert(std::ranges::bidirectional_range<T>);
        static_assert(std::ranges::random_access_range<T>);

        static_assert(geom::table<T>);

        void _default_constructor() const
        {
                T t;
        }

        void _constructor_1() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                TEST(t.offset() == o);
                TEST(t.size() == s);
        }

        void _constructor_2() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                auto x = samples.make<mapped_type>(1, 10);
                T t(o, s, x);
                TEST(t.offset() == o);
                TEST(t.size() == s);
        }

        void _copy_constructor() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u(t);
        }

        void _move_constructor() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u(std::move(t));
        }

        void _copy_assignment() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u;
                u = t;
        }

        void _move_assignment() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                T u;
                u = std::move(t);
        }

        void _list_assignment() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                t = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        }

        void _iterator_distance() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                TEST(std::distance(t.begin(), t.end()) ==
                                std::accumulate(std::ranges::begin(s),
                                        std::ranges::end(s),
                                        1,
                                        std::multiplies{}));
        }

        void _iterator_path() const
        {
                auto o = samples.make<key_type>(0, 10);
                auto s = samples.make<key_type>(1, 10);
                T t(o, s);
                auto j = t.begin();
                for (auto p : geom::frame(key_type{}, s))
                        TEST(geom::path(t.begin(), j++) == p);
        }

        void _iterator_value_1() const
        {
                auto o = samples.make<key_type>(0, 5);
                auto s = samples.make<key_type>(1, 5);
                T t(o, s);
                std::vector<mapped_type> v(t.capacity());
                //std::ranges::generate(v, [](){ return samples.make<mapped_type>(); });
                std::iota(std::ranges::begin(v), std::ranges::end(v), mapped_type{0});
                std::ranges::copy(v, t.begin());
                try {
                        auto j = v.begin();
                        for (auto p : geom::frame(o, o + s))
                                TEST(*j++ == t[p]);
                } catch (std::exception& ex) {
                        std::cerr << ex.what() << "\n";
                        std::cerr << " | offset: " << o << "\n";
                        std::cerr << " | size: " << s << "\n";
                        std::cerr << " | vector:";
                        print_range(std::cerr, v);
                        std::cerr << " | table:";
                        print_range(std::cerr, t);
                }
        }

        void _iterator_value_2() const
        {
                auto o = samples.make<key_type>(0, 5);
                auto s = samples.make<key_type>(1, 5);
                T t(o, s);
                std::vector<mapped_type> v(t.capacity());
                //std::ranges::generate(t, [](){ return samples.make<mapped_type>(); });
                std::iota(std::ranges::begin(t), std::ranges::end(t), mapped_type{0});
                std::ranges::copy(t, v.begin());
                try {
                        auto j = v.begin();
                        for (auto p : geom::frame(o, o + s))
                                TEST(*j++ == t[p]);
                } catch (std::exception& ex) {
                        std::cerr << ex.what() << "\n";
                        std::cout << " | offset: " << o << "\n";
                        std::cout << " | size: " << s << "\n";
                        std::cout << " | table:";
                        print_range(std::cout, t);
                        std::cout << " | vector:";
                        print_range(std::cout, v);
                }
        }

        void operator()() const
        {
                for (size_t n = 0; n != 1000; ++n) {
                        _default_constructor();
                        _constructor_1();
                        _constructor_2();
                        _copy_constructor();
                        _move_constructor();
                        _copy_assignment();
                        _move_assignment();
                        _list_assignment();
                        _iterator_distance();
                        _iterator_path();
                        _iterator_value_1();
                        _iterator_value_2();
                }
        }
};


int main(int argc, char* argv[])
{
        Test<geom::Sparse_table<geom::Vec<short, 2>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<int, 2>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<long, 2>, int>>{}();

        Test<geom::Sparse_table<geom::Vec<short, 3>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<int, 3>, int>>{}();
        Test<geom::Sparse_table<geom::Vec<long, 3>, int>>{}();
}
