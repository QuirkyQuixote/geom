
#include <cassert>
#include <random>

#include "geom/math.h"

template<class T, class U> void assert_identical(const T& l, const U& r)
{
        static_assert(std::is_same_v<T, U>);
        assert(l == r);
}

struct Samples {
        std::random_device rd{};
        std::mt19937 gen{rd()};

        template<std::integral I> auto num()
        {
                std::uniform_int_distribution<I> distr;
                return distr(gen);
        }

        template<class T> auto vec()
        { return geom::Vec<T, 3>(num<T>(), num<T>(), num<T>()); }
};

template<class T> void test_unary(Samples& samples)
{
        // Test equality comparison between the same object

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                assert(l == l);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                assert(!(l != l));
        }

        // Test unary operators

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto s = decltype(l)(l.x + 1, l.y + 1, l.z + 1);
                assert_identical(++l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto s = decltype(l)(l.x - 1, l.y - 1, l.z - 1);
                assert_identical(--l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto p = l;
                auto s = decltype(l)(l.x + 1, l.y + 1, l.z + 1);
                assert_identical(l++, p);
                assert_identical(l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto p = l;
                auto s = decltype(l)(l.x - 1, l.y - 1, l.z - 1);
                assert_identical(l--, p);
                assert_identical(l, s);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                assert_identical(+l, geom::make_vec(+l.x, +l.y, +l.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                assert_identical(-l, geom::make_vec(-l.x, -l.y, -l.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                assert_identical(~l, geom::make_vec(~l.x, ~l.y, ~l.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                assert_identical(!l, geom::make_vec(!l.x, !l.y, !l.z));
        }
}

template<class T, class U> void test_binary(Samples& samples)
{
        // Test equality comparison with a different object

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert((l == r) == (l.x == r.x && l.y == r.y && l.z == r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert((l != r) == (l.x != r.x || l.y != r.y || l.z != r.z));
        }

        // Test compound assigment with scalar

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x + r, l.y + r, l.z + r);
                assert_identical(l += r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x - r, l.y - r, l.z - r);
                assert_identical(l -= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x * r, l.y * r, l.z * r);
                assert_identical(l *= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                auto m = decltype(l)(l.x / r, l.y / r, l.z / r);
                assert_identical(l /= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                auto m = decltype(l)(l.x % r, l.y % r, l.z % r);
                assert_identical(l %= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x | r, l.y | r, l.z | r);
                assert_identical(l |= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x & r, l.y & r, l.z & r);
                assert_identical(l &= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x ^ r, l.y ^ r, l.z ^ r);
                assert_identical(l ^= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x << r, l.y << r, l.z << r);
                assert_identical(l <<= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                auto m = decltype(l)(l.x >> r, l.y >> r, l.z >> r);
                assert_identical(l >>= r, m);
        }

        // Test compound assignment with vector

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x + r.x, l.y + r.y, l.z + r.z);
                assert_identical(l += r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x - r.x, l.y - r.y, l.z - r.z);
                assert_identical(l -= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x * r.x, l.y * r.y, l.z * r.z);
                assert_identical(l *= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                auto m = decltype(l)(l.x / r.x, l.y / r.y, l.z / r.z);
                assert_identical(l /= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                auto m = decltype(l)(l.x % r.x, l.y % r.y, l.z % r.z);
                assert_identical(l %= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x | r.x, l.y | r.y, l.z | r.z);
                assert_identical(l |= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x & r.x, l.y & r.y, l.z & r.z);
                assert_identical(l &= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x ^ r.x, l.y ^ r.y, l.z ^ r.z);
                assert_identical(l ^= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x << r.x, l.y << r.y, l.z << r.z);
                assert_identical(l <<= r, m);
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                auto m = decltype(l)(l.x >> r.x, l.y >> r.y, l.z >> r.z);
                assert_identical(l >>= r, m);
        }

        // Test binary operation with scalar

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l + r, geom::make_vec(l.x + r, l.y + r, l.z + r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l - r, geom::make_vec(l.x - r, l.y - r, l.z - r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l * r, geom::make_vec(l.x * r, l.y * r, l.z * r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                assert_identical(l / r, geom::make_vec(l.x / r, l.y / r, l.z / r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                if (r == 0) r = 1;
                assert_identical(l % r, geom::make_vec(l.x % r, l.y % r, l.z % r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l | r, geom::make_vec(l.x | r, l.y | r, l.z | r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l & r, geom::make_vec(l.x & r, l.y & r, l.z & r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l ^ r, geom::make_vec(l.x ^ r, l.y ^ r, l.z ^ r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l << r, geom::make_vec(l.x << r, l.y << r, l.z << r));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.num<U>();
                assert_identical(l >> r, geom::make_vec(l.x >> r, l.y >> r, l.z >> r));
        }

        // Test binary operation with vector

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l + r, geom::make_vec(l.x + r.x, l.y + r.y, l.z + r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l - r, geom::make_vec(l.x - r.x, l.y - r.y, l.z - r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l * r, geom::make_vec(l.x * r.x, l.y * r.y, l.z * r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                assert_identical(l / r, geom::make_vec(l.x / r.x, l.y / r.y, l.z / r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                if (r.x == 0) r.x = 1;
                if (r.y == 0) r.y = 1;
                if (r.z == 0) r.z = 1;
                assert_identical(l % r, geom::make_vec(l.x % r.x, l.y % r.y, l.z % r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l | r, geom::make_vec(l.x | r.x, l.y | r.y, l.z | r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l & r, geom::make_vec(l.x & r.x, l.y & r.y, l.z & r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l ^ r, geom::make_vec(l.x ^ r.x, l.y ^ r.y, l.z ^ r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l << r, geom::make_vec(l.x << r.x, l.y << r.y, l.z << r.z));
        }

        for (int i = 0; i < 1000; ++i) {
                auto l = samples.vec<T>();
                auto r = samples.vec<U>();
                assert_identical(l >> r, geom::make_vec(l.x >> r.x, l.y >> r.y, l.z >> r.z));
        }

}

int main(int argc, char* argv[])
{
        Samples samples;

        {
                geom::Vec<int, 3> v;
                assert(v.x == 0);
                assert(v.y == 0);
                assert(v.z == 0);
        }

        // Test iterator

        {
                geom::Vec<int, 3> v;
                assert(std::addressof(v.x) == geom::begin(v) + 0);
                assert(std::addressof(v.y) == geom::begin(v) + 1);
                assert(std::addressof(v.z) == geom::begin(v) + 2);
        }

        // Test element access

        {
                geom::Vec<int, 3> v;
                assert(std::addressof(v.x) == std::addressof(v[0]));
                assert(std::addressof(v.y) == std::addressof(v[1]));
                assert(std::addressof(v.z) == std::addressof(v[2]));
        }

        test_unary<char>(samples);
        test_unary<int>(samples);
        test_unary<long long>(samples);

        test_binary<char, char>(samples);
        test_binary<char, int>(samples);
        test_binary<char, long long>(samples);
        test_binary<int, char>(samples);
        test_binary<int, int>(samples);
        test_binary<int, long long>(samples);
        test_binary<long long, char>(samples);
        test_binary<long long, int>(samples);
        test_binary<long long, long long>(samples);

        return 0;
}

