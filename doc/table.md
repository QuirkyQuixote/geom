
# geom::Table

Defined in [`geom/table.h`](../geom/table.h).

```cpp
template<class Key, class T> Table
```

`geom::Table` is a N-dimensional table that can be addressed with
[`geom::Vec`](vec.md) values.

## Member Types

```cpp
using Container = std::vector<T>;
using value_type = T;
using size_type = Diff;
using difference_type = Diff;
using reference = typename Container::reference;
using const_reference = typename Container::const_reference;
using pointer = typename Container::pointer;
using const_pointer = typename Container::const_pointer;
using iterator = Table_iterator<typename Container::iterator, Table_size<Diff>>;
using const_iterator = Table_iterator<typename Container::const_iterator, Table_size<Diff>>;
using reverse_iterator = Table_iterator<typename Container::reverse_iterator, Table_size<Diff>>;
using const_reverse_iterator = Table_iterator<typename Container::const_reverse_iterator, Table_size<Diff>>;
using sentinel = Table_sentinel;
```

## Member Functions

### Constructors

```cpp
Table();
Table(const size_type& size, const T& value);
Table(const size_type& size);
Table(const Table&) = default;
Table(Table&&) = default;
```

Constructs a new `Table` from a number of data sources:

1. an empty table with no data on it,
2. a table of the given `size` filled with `value`,
3. a table of the given `size`,
4. a copy of another table,
5. a table that steals the contents of another.

### Assignment

```cpp
Table& operator=(const Table&);
Table& operator=(Table&&);
```

Assigns the contents of one `Table` to another.

### Iterators

```cpp
iterator begin();
const_iterator begin() const;
const_iterator cbegin() const;
```

```cpp
sentinel end();
sentinel end() const;
sentinel cend() const;
```

```cpp
reverse_iterator rbegin();
const_reverse_iterator rbegin() const;
const_reverse_iterator crbegin() const;
```

```cpp
sentinel rend();
sentinel rend() const;
sentinel crend() const;
```

### Element Access

```cpp
reference operator[](const size_type& n);
const_reference operator[](const size_type& n) const;
```

Return a reference to the element at the specified location. No bound checking
is performed.

### Table Size

```cpp
size_type size() const;
```

Return the size of the table.

```cpp
void resize(const size_type& size);
void resize(const size_type& size, const T& value)
```

Resize the table. The order of elements will not be preserved.

### Container access

```cpp
constexpr Container& base();
constexpr const Container& base();
```

Return a reference to the underlying sequence of elements.
