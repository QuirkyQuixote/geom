
# geom::Slice

Defined in [`geom/slice.h`](../geom/slice.h).

```cpp
template<class Base> class Slice;
```

`geom::Slice` is a view for Tables
([`geom::Table`](table.md), [`geom::Sparse_table`](sparse_table.md)) that
includes only those elments contained in a specific box [`geom::Box`](box.md)
provided when the slice is created.

## Member Types

```cpp
using iterator = Slice_iterator<Base>;
using sentinel = Slice_sentinel;
using difference_type = typename Base::difference_type;
```

## Member Functions

### Constructors

```cpp
constexpr Slice() = default;
constexpr Slice(const Base& base, const difference_type& size);
constexpr Slice(const Slice& other);
constexpr Slice(Slice&& other);
```

Construct a slice from a variety of data sources:

1. default constructor,
2. an iterator to the starting position on the table and the size of the box
   that defines the slice limits,
3. Copy another slice.
4. Acquire state of another slice.

### Assignment

```cpp
constexpr Slice& operator=(const Slice& other) = default;
constexpr Slice& operator=(Slice&& other) = default;
```

Copies state of another slice.

### Iterators

```cpp
constexpr iterator begin();
constexpr sentinel end();
```

## Non-member Functions

```cpp
template<class Table, class Diff, std::enable_if_t<is_vec_v<Diff>, int> = 0>
auto slice(Table& t, const Diff& off, const Diff& size)
template<class Table, class Diff, std::enable_if_t<is_vec_v<Diff>, int> = 0>
auto slice(const Table& t, const Diff& off, const Diff& size)
```

Construct a slice from a table and a pair of vectors that define the minimum
and maximum corners of the box.

```cpp
template<class Table, class Box, std::enable_if_t<is_box_v<Box>, int> = 0>
auto slice(Table& t, const Box& box)
template<class Table, class Box, std::enable_if_t<is_box_v<Box>, int> = 0>
auto slice(const Table& t, const Box& box)
```

Construct a slice from a table and a box.

