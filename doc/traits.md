
# Type Support

Defined in [`geom/traits.h`](../geom/traits.h).

Compile-time template-based interface to query type properties.

## Checks whether a type is Num

```cpp
template<class T> struct is_num;
template<class T> inline constexpr bool is_num_v = is_num<T>::value;
```

## Checks whether a type is Range

```cpp
template<class T> struct is_range;
template<class T> inline constexpr bool is_range_v = is_range<T>::value;
```

## Checks whether a type is Vec

```cpp
template<class T> struct is_vec;
template<class T> inline constexpr bool is_vec_v = is_vec<T>::value;

template<class T> struct is_vec2;
template<class T> inline constexpr bool is_vec2_v = is_vec2<T>::value;

template<class T> struct is_vec3;
template<class T> inline constexpr bool is_vec3_v = is_vec3<T>::value;
```

## Checks whether a type is Pos

```cpp
template<class T> struct is_pos;
template<class T> inline constexpr bool is_pos_v = is_pos<T>::value;

template<class T> struct is_pos2;
template<class T> inline constexpr bool is_pos2_v = is_pos2<T>::value;

template<class T> struct is_pos3;
template<class T> inline constexpr bool is_pos3_v = is_pos3<T>::value;
```

## Checks whether a type is Box

```cpp
template<class T> struct is_box;
template<class T> inline constexpr bool is_box_v = is_box<T>::value;

template<class T> struct is_box2;
template<class T> inline constexpr bool is_box2_v = is_box2<T>::value;

template<class T> struct is_box3;
template<class T> inline constexpr bool is_box3_v = is_box3<T>::value;
```

## Checks whether a type is Table

```cpp
template<class T> struct is_table;
template<class T> inline constexpr bool is_table_v = is_table<T>::value;

template<class T> struct is_table2;
template<class T> inline constexpr bool is_table2_v = is_table2<T>::value;

template<class T> struct is_table3;
template<class T> inline constexpr bool is_table3_v = is_table3<T>::value;
```

## Number of Dimensions

```cpp
template<class T> struct dimension {};
template<class T> inline constexpr std::size_t dimension_v = dimension<T>::value;
```

For simple types this may be the same as size().
For tables, it is the size of their size.

## Common Type

```cpp
template<class T1, class T2> struct common_type {};
template<class T1, class T2> using common_type_t = typename common_type<T1, T2>::type;
```

