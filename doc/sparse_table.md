
# geom::Sparse_table

Defined in [`geom/sparse_table.h`](../geom/sparse_table.h).

```cpp
template<class Key, class T> Sparse_table
```

`geom::Sparse_table` is a N-dimensional table that can be addressed with
[`geom::Vec`](vec.md) values. Unlike [`geom::Table`](table.md) that stores data
in a contiguous array, `Sparse_table` uses a [`geom::Tree`](tree.md).

## Member Types

```cpp
using Self = Sparse_table<T, Diff>;
using Base = Tree<Diff, T>;
using value_type = T;
using size_type = Diff;
using difference_type = Diff;
using reference = Sparse_table_reference<Base>;
using const_reference = const value_type&;
using pointer = value_type*;
using const_pointer = const value_type*;
using iterator = Sparse_table_iterator<Self, reference, pointer>;
using const_iterator = Sparse_table_iterator<Self, const_reference, const_pointer>;
using sentinel = Sparse_table_sentinel;
```

## Member Functions

### Constructors

```cpp
Sparse_table();
Sparse_table(const size_type& size, const T& value);
Sparse_table(const Sparse_table&) = default;
Sparse_table(Sparse_table&&) = default;
```

Constructs a new `Sparse_table` from a number of data sources:

1. an empty table with no data on it,
2. a table of the given `size` filled with `value`,
3. a copy of another table,
4. a table that steals the contents of another.

### Assignment

```cpp
Sparse_table& operator=(const Sparse_table&);
Sparse_table& operator=(Sparse_table&&);
```

Assigns the contents of one `Sparse_table` to another.

### Iterators

```cpp
iterator begin();
const_iterator begin() const;
const_iterator cbegin() const;
```

```cpp
sentinel end();
sentinel end() const;
sentinel cend() const;
```

```cpp
reverse_iterator rbegin();
const_reverse_iterator rbegin() const;
const_reverse_iterator crbegin() const;
```

```cpp
sentinel rend();
sentinel rend() const;
sentinel crend() const;
```

### Element Access

```cpp
reference operator[](const size_type& n);
const_reference operator[](const size_type& n) const;
```

Return a reference to the element at the specified location. No bound checking
is performed.

### Table Size

```cpp
size_type size() const;
```

Return the size of the table.

```cpp
void resize(const size_type& new_size);
```

Resize the table. The order of elements will be preserved. Additional elements
will be assigned the default value this table was built with.

### Container access

```cpp
constexpr Container& base();
constexpr const Container& base();
```

Return a reference to the underlying sequence of elements.
