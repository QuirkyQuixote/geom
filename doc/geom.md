
# geom

The geom library provides the following:

## Strongly Typed Space

Every template in this section has three arguments:

1. `Rep`, the type of its internal representation, usually an integer.
2. `Tag`, that makes types unique: only those types with the same tag can
   interact, and it's usually an empty struct.
3. `Ratio`, an `std::ratio` object that determines the scale of the type
   relative to other objects with the same `Tag`.

### Classes

- [`geom::Num`](num.md), a scalar type,
- [`geom::Vec`](vec.md), a vector to describe directions and distances,
- [`geom::Interval`](interval.md), a range between two scalar values,
- [`geom::Box`](box.md), an axis-aligned box,

## Containers

All containers in this library are "tables"; very similar to STL sequence
containers, but addressable with values of type `Vec`

### Classes

- [`geom::Table`](table.md), a plain table stored as a contiguous array on
  memory addressed by [`geom::Vec`](vec.md) instances.
- [`geom::Sparse_table`](sparse_table.md), a table implemented as
  space-partitioning tree.
- [`geom::Slice`](slice.md), a view for tables that contains only the values
  contained in a given box.

## Implementation Details

These are the backend that allows the rest of the library to work.

- [`Type Support`](traits.md), compile-time template interface to query type
  properties.
- [`Algorithms`](algorithm.md), algorithms that operate on the types defined
  elsewhere.

### Classes

- [`geom::Tree`](tree.md), an efficient quadtree and octree used in the
  implementation of [`geom::Sparse_table`](sparse_table.md).
- [`geom::Pool`](pool.md), a pool of objects addressed by index that guarantees
  contiguous storage, used in the implementation of [`geom::Tree`](tree.md).
