
# geom::Vec

Defined in [`geom/vec.h`](../geom/vec.h).

```cpp
template<class T, size_t N> struct Vec;
```

`geom::Vec` is a class template that models a N-dimensional vector.

## Member Types

```cpp
using value_type = T;
using reference = T&;
using const_reference = const T&;
using pointer = T*;
using const_pointer = const T*;
using iterator = T*;
using const_iterator = const T*;
```

## Member objects

Template specializations are provided for `N == 1`, `N == 2` and `N == 3`;
each has a different number of member values.

```cpp
T x;
T y;
T z;
...
```

## Member Functions

### Constructors

```cpp
constexpr Vec() = default;
constexpr Vec(const Vec&) = default;
constexpr Vec(Vec&&) = default;
constexpr Vec(T x, T y, T z, ...);
template<std::integral I> constexpr Vec(I x, I y, I z, ...);
template<class U> constexpr Vec(const Vec<U, N>& v);
```

Constructs a new `Vec` from:

1. nothing, the values are uninitialized;
2. another vec that is copied;
3. another vec that is moved;
4. a list of values to be copied to each field;
5. a list of values to initialize each field;
6. Another Vec whose representation is transformed; if precision is lost, the
   number is rounded towards zero.

### Assignment

```cpp
Vec& operator=(const Vec&) = default;
Vec& operator=(Vec&&) = default;
```

Assigns the contents of one `Vec` to another.

### Iterators

```cpp
constexpr iterator begin();
constexpr const_iterator begin() const;
constexpr const_iterator cbegin() const;

constexpr iterator end();
constexpr const_iterator end() const;
constexpr const_iterator cend() const;

constexpr iterator rbegin();
constexpr const_iterator rbegin() const;
constexpr const_iterator crbegin() const;

constexpr iterator rend();
constexpr const_iterator rend() const;
constexpr const_iterator crend() const;
```

Return iterators to the beginning or the end of the range of values of a given
`Vec` instance.

### Element Access

```cpp
constexpr reference operator[](size_t n);
constexpr const_reference operator[](size_t n) const;
```

Returns a reference to the nth element (`x` is `0`, `y` is `1`, etc.) of the
`Vec`; doesn't perform bounds checking.

```cpp
reference at(size_t n);
const_reference at(size_t n) const;
```

Returns a reference to the nth element (`x` is `0`, `y` is `1`, etc.) of the
`Vec`; if `n` is not within range it throws `std::out_of_range`.

### Unary Operations

```cpp
constexpr Vec<decltype(+T()), N> operator+() const;
constexpr Vec<decltype(-T()), N> operator-() const;
constexpr Vec<decltype(~T()), N> operator~() const;
constexpr Vec<decltype(!T()), N> operator!() const;
```

Applies an unary operation to each element of the vector, and returns a new
vector with the results.

### Compound Assignment

```cpp
template<class U> constexpr Vec& operator+=(U r);
template<class U> constexpr Vec& operator-=(U r);
template<class U> constexpr Vec& operator*=(U r);
template<class U> constexpr Vec& operator/=(U r);
template<class U> constexpr Vec& operator%=(U r);
template<class U> constexpr Vec& operator&=(U r);
template<class U> constexpr Vec& operator|=(U r);
template<class U> constexpr Vec& operator^=(U r);
template<class U> constexpr Vec& operator<<=(U r);
template<class U> constexpr Vec& operator>>=(U r);
```

Applies compound assignment to each element of `l` with `r`.

```cpp
template<class U> constexpr Vec& operator+=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator-=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator*=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator/=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator%=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator&=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator|=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator^=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator<<=(const Vec<U, N>& r);
template<class U> constexpr Vec& operator>>=(const Vec<U, N>& r);
```

Applies compound assignment to each element of `l` with the corresponding
element from `r`.

### Binary Operators

```cpp
template<class U> constexpr Vec<decltype(T() + U()), N> operator+(const U& r) const;
template<class U> constexpr Vec<decltype(T() - U()), N> operator-(const U& r) const;
template<class U> constexpr Vec<decltype(T() * U()), N> operator*(const U& r) const;
template<class U> constexpr Vec<decltype(T() / U()), N> operator/(const U& r) const;
template<class U> constexpr Vec<decltype(T() % U()), N> operator%(const U& r) const;
template<class U> constexpr Vec<decltype(T() | U()), N> operator|(const U& r) const;
template<class U> constexpr Vec<decltype(T() & U()), N> operator&(const U& r) const;
template<class U> constexpr Vec<decltype(T() ^ U()), N> operator^(const U& r) const;
template<class U> constexpr Vec<decltype(T() << U()), N> operator<<(const U& r) const;
template<class U> constexpr Vec<decltype(T() >> U()), N> operator>>(const U& r) const;
```

Applies a binary operator to each element of `l` with `r`.

```cpp
template<class U> constexpr Vec<decltype(T() + U()), N> operator+(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() - U()), N> operator-(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() * U()), N> operator*(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() / U()), N> operator/(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() % U()), N> operator%(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() | U()), N> operator|(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() & U()), N> operator&(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() ^ U()), N> operator^(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() << U()), N> operator<<(const Vec<U, N>& r) const;
template<class U> constexpr Vec<decltype(T() >> U()), N> operator>>(const Vec<U, N>& r) const;
```

Applies a binary operator to each element of `l` with the corresponding
element from `r`.

## Non-member Functions

### Comparison

```cpp
template<class T, class U, size_t N> constexpr bool operator==(const Vec<T, N>& l, const Vec<U, N>& r);
template<class T, class U, size_t N> constexpr bool operator!=(const Vec<T, N>& l, const Vec<U, N>& r);
```

Compares two `Vec`; requires `T` and `U` to be comparable.

### Type-deduced Construction

```cpp
template<class... T> requires all_same_v<T...>
constexpr Vec<pack_head_t<T>, sizeof...(Args)> make_vec(T... args);
```

Constructs a `Vec` object, deducing the target type from the types of the
arguments.

