
# geom::Tree

Defined in [`geom/tree.h`](../geom/tree.h)

```cpp
template<class Key, class T> struct Tree;
```

`geom::Tree` implements a recursive space-partitioning tree (a quadtree in two
dimensions and an octree in three) that contains instances of `T` and can be
addressed by `Key`, that is supposed to be a instantiation of
[`geom::Vec`](vec.md).

It is used as container by [`geom::Sparse_table`](sparse_table.md).

## Member Types

```cpp
using key_type = Key;
using value_type = T;
using reference = T&;
using const_reference = const T&;
using pointer = T*;
using const_pointer = const T*;
using size_type = size_t;
```

## Member Functions

### Constructors

```cpp
Tree(size_type size, const value_type& value)
Tree() = default;
Tree(const Tree&) = default;
Tree(Tree&&) = default;
```

Construct a tree from a variety of data sources

1. A tree with enough depth to represent a bidimensional table of width and
   height size, with all elements set to value.
2. An empty tree, with no values.
3. A copy of another tree.
4. A tree that steals the internal state of another.

### Assignment

```cpp
Tree& operator=(const Tree&) = default;
Tree& operator=(Tree&&) = default;
```

Assign the contents of one tree to another

### Lookup

```cpp
const_reference find(const key_type& key)
```

Find element at given position

### Insertion

```cpp
void insert(const key_type& key, const value_type& value)
```

Replace element at position key with value.

### Resizing

```cpp
void resize(size_type size);
```

Resize the tree to be able to hold a table of elements of width and height
equal to size. Newly created nodes (if any) will be assigned the value that was
passed to the constructor.

