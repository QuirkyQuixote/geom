
# geom::Box

Defined in [`geom/box.h`](../geom/box.h).

```cpp
template<class T, size_t N> struct Box;
```

`geom::Box` is a class template that models an axis-aligned N-dimensional box.

## Member Types

```cpp
using value_type = Interval<T>;
using reference = Interval<T>&;
using const_reference = const Interval<T>&;
using pointer = Interval<T>*;
using const_pointer = const Interval<T>*;
using iterator = Interval<T>*;
using const_iterator = const Interval<T>*;
```

## Member objects

Template specializations are provided for `N == 2` and `N == 3`; each of one
has a different number of member values.

```cpp
T l, r;
T u, d;
T f, b;
...
```

## Member Functions

### Constructors

```cpp
constexpr Box() = default;
constexpr Box(const Box&) = default;
constexpr Box(Box&&) = default;
constexpr Box(const Interval<T>& x, const Interval<T>& y, const Interval<T>& z);
constexpr Box(T l, T r, T u, T d, T f, T b...);
template<std::integral I> constexpr Box(I l, I r, I u, I d, I f, I b...);
constexpr Box(const Vec<U, N>& v);
constexpr Box(const Vec<U, N>& min, const Vec<U, N>& max);
template<class U> constexpr Box(const Box<U, N>& b);
```

Constructs a new `Box` from:

1. nothing, the values are uninitialized;
2. another box that is copied;
3. another box that is moved;
4. a list of values to be copied ot each field;
5. a list of values, two to create each field;
6. a vector to initialize a box of size zero;
7. two vectors that define the minimum and maximum corners.
8. Another Box whose representation is transformed; if precision is lost, the
   number is rounded towards zero.

### Assignment

```cpp
Box& operator=(const Box&) = default;
Box& operator=(Box&&) = default;
```

Assigns the contents of one `Box` to another.

### Iterators

```cpp
constexpr iterator begin();
constexpr const_iterator begin() const;
constexpr const_iterator cbegin() const;

constexpr iterator end();
constexpr const_iterator end() const;
constexpr const_iterator cend() const;

constexpr iterator rbegin();
constexpr const_iterator rbegin() const;
constexpr const_iterator crbegin() const;

constexpr iterator rend();
constexpr const_iterator rend() const;
constexpr const_iterator crend() const;
```

Return iterators to the beginning or the end of the range of values of a given
`Box` instance.

### Element Access

```cpp
constexpr reference operator[](size_t n);
constexpr const_reference operator[](size_t n) const;
```

Returns a reference to the nth element (`x` is `0`, `y` is `1`, etc.) of the
`Box`; doesn't perform bounds checking.

```cpp
reference at(size_t n);
const_reference at(size_t n) const;
```

Returns a reference to the nth element (`x` is `0`, `y` is `1`, etc.) of the
`Box`; if `n` is not within range it throws `std::out_of_range`.

```cpp
constexpr Vec<T, N> min() const;
constexpr Vec<T, N> max() const;
constexpr Vec<T, N> size() const;
```

1. returns the corner of the box corresponding to its smaller values,
2. returns the corner of the box corresponding to its greater values,
3. equivalent to `max() - min()`.

### Unary Operations

```cpp
constexpr Box<decltype(+T()), N> operator+() const;
constexpr Box<decltype(-T()), N> operator-() const;
constexpr Box<decltype(~T()), N> operator~() const;
constexpr Box<decltype(!T()), N> operator!() const;
```

Applies an unary operation to each element of `l`, and returns a new box
with the results.

### Compound Assignment

```cpp
template<class U> constexpr Box& operator+=(U r);
template<class U> constexpr Box& operator-=(U r);
template<class U> constexpr Box& operator*=(U r);
template<class U> constexpr Box& operator/=(U r);
template<class U> constexpr Box& operator%=(U r);
template<class U> constexpr Box& operator&=(U r);
template<class U> constexpr Box& operator|=(U r);
template<class U> constexpr Box& operator^=(U r);
template<class U> constexpr Box& operator<<=(U r);
template<class U> constexpr Box& operator>>=(U r);
```

Applies compound assignment to each element of `l` with `r`.

```cpp
template<class U> constexpr Box& operator+=(const Vec<U, N>& r);
template<class U> constexpr Box& operator-=(const Vec<U, N>& r);
template<class U> constexpr Box& operator*=(const Vec<U, N>& r);
template<class U> constexpr Box& operator/=(const Vec<U, N>& r);
template<class U> constexpr Box& operator%=(const Vec<U, N>& r);
template<class U> constexpr Box& operator&=(const Vec<U, N>& r);
template<class U> constexpr Box& operator|=(const Vec<U, N>& r);
template<class U> constexpr Box& operator^=(const Vec<U, N>& r);
template<class U> constexpr Box& operator<<=(const Vec<U, N>& r);
template<class U> constexpr Box& operator>>=(const Vec<U, N>& r);
```

Applies compound assignment to each element of `l` with the corresponding
element from `r`.

```cpp
template<class U> constexpr Box& operator+=(const Box<U, N>& r);
template<class U> constexpr Box& operator-=(const Box<U, N>& r);
template<class U> constexpr Box& operator*=(const Box<U, N>& r);
template<class U> constexpr Box& operator/=(const Box<U, N>& r);
template<class U> constexpr Box& operator%=(const Box<U, N>& r);
template<class U> constexpr Box& operator&=(const Box<U, N>& r);
template<class U> constexpr Box& operator|=(const Box<U, N>& r);
template<class U> constexpr Box& operator^=(const Box<U, N>& r);
template<class U> constexpr Box& operator<<=(const Box<U, N>& r);
template<class U> constexpr Box& operator>>=(const Box<U, N>& r);
```

Applies compound assignment to each element of `l` with the corresponding
element from `r`.

### Binary Operators

```cpp
template<class U> constexpr Box<decltype(T() + U()), N> operator+(U r) const
template<class U> constexpr Box<decltype(T() - U()), N> operator-(U r) const
template<class U> constexpr Box<decltype(T() * U()), N> operator*(U r) const
template<class U> constexpr Box<decltype(T() / U()), N> operator/(U r) const
template<class U> constexpr Box<decltype(T() % U()), N> operator%(U r) const
template<class U> constexpr Box<decltype(T() | U()), N> operator|(U r) const
template<class U> constexpr Box<decltype(T() & U()), N> operator&(U r) const
template<class U> constexpr Box<decltype(T() ^ U()), N> operator^(U r) const
template<class U> constexpr Box<decltype(T() << U()), N> operator<<(U r) const
template<class U> constexpr Box<decltype(T() >> U()), N> operator>>(U r) const
```

Applies a binary operator to each element of `l` with `r`.

```cpp
template<class U> constexpr Box<decltype(T() + U()), N> operator+(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() - U()), N> operator-(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() * U()), N> operator*(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() / U()), N> operator/(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() % U()), N> operator%(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() | U()), N> operator|(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() & U()), N> operator&(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() ^ U()), N> operator^(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() << U()), N> operator<<(const Vec<U, T>& r) const
template<class U> constexpr Box<decltype(T() >> U()), N> operator>>(const Vec<U, T>& r) const
```

Applies a binary operator to each element of `l` with the corresponding
element from `r`.

```cpp
template<class U> constexpr Box<decltype(T() + U()), N> operator+(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() - U()), N> operator-(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() * U()), N> operator*(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() / U()), N> operator/(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() % U()), N> operator%(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() | U()), N> operator|(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() & U()), N> operator&(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() ^ U()), N> operator^(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() << U()), N> operator<<(const Box<U, T>& r) const
template<class U> constexpr Box<decltype(T() >> U()), N> operator>>(const Box<U, T>& r) const
```

Applies a binary operator to each element of `l` with the corresponding
element from `r`.

## Non-member Functions

### Comparison

```cpp
template<class T, class U, size_t N> constexpr bool operator==(const Box<T, N>& l, const Box<U, N>& r);
template<class T, class U, size_t N> constexpr bool operator!=(const Box<T, N>& l, const Box<U, N>& r);
```

Compares two `Box`; requires `T` and `U` to be comparable.

### Type-deduced Construction

```cpp
template<class... T> requires all_same_v<T...>
constexpr Box<pack_head_t<T...>, sizeof...(T) / 2> make_box(T... args);

template<class... T> requires all_same_v<T...>
constexpr Box<pack_head_t<T...>, sizeof...(T)> make_box(Interval<T>... args);

template<class T, size_t N>
constexpr Box<T, N> make_box(const Vec<T, N>& min);

template<class T, size_t N>
constexpr Box<T, N> make_box(const Vec<T, N>& min, const Vec<T, N>& max);
```

Constructs a `Box` object, deducing the target type from the types of the
arguments.

### Border-finding

```cpp
template<class... Args, class T>
constexpr auto box_point(const Box<T, sizeof...(Args)>& b);
```

Return the vector whose position is given by the box `b` and the ratios in
`Args...`; e.g.: if the ratio for the first coordinate is 1/2, the value of
the resulting verctor for that coordinate is exactly in the middle of the box.

```cpp
template<class T> constexpr auto top_left(const Box<T, 2>& b) { return box_point<min, min>(b); }
template<class T> constexpr auto top(const Box<T, 2>& b) { return box_point<mid, min>(b); }
template<class T> constexpr auto top_right(const Box<T, 2>& b) { return box_point<max, min>(b); }
template<class T> constexpr auto left(const Box<T, 2>& b) { return box_point<min, mid>(b); }
template<class T> constexpr auto center(const Box<T, 2>& b) { return box_point<mid, mid>(b); }
template<class T> constexpr auto right(const Box<T, 2>& b) { return box_point<max, mid>(b); }
template<class T> constexpr auto bottom_left(const Box<T, 2>& b) { return box_point<min, max>(b); }
template<class T> constexpr auto bottom(const Box<T, 2>& b) { return box_point<mid, max>(b); }
template<class T> constexpr auto bottom_right(const Box<T, 2>& b) { return box_point<max, max>(b); }
```

```cpp
template<class T> constexpr auto front_top_left(const Box<T, 3>& b) { return box_point<min, min, min>(b); }
template<class T> constexpr auto front_top(const Box<T, 3>& b) { return box_point<mid, min, min>(b); }
template<class T> constexpr auto front_top_right(const Box<T, 3>& b) { return box_point<max, min, min>(b); }
template<class T> constexpr auto front_left(const Box<T, 3>& b) { return box_point<min, mid, min>(b); }
template<class T> constexpr auto front(const Box<T, 3>& b) { return box_point<mid, mid, min>(b); }
template<class T> constexpr auto front_right(const Box<T, 3>& b) { return box_point<max, mid, min>(b); }
template<class T> constexpr auto front_bottom_left(const Box<T, 3>& b) { return box_point<min, max, min>(b); }
template<class T> constexpr auto front_bottom(const Box<T, 3>& b) { return box_point<mid, max, min>(b); }
template<class T> constexpr auto front_bottom_right(const Box<T, 3>& b) { return box_point<max, max, min>(b); }

template<class T> constexpr auto top_left(const Box<T, 3>& b) { return box_point<min, min, mid>(b); }
template<class T> constexpr auto top(const Box<T, 3>& b) { return box_point<mid, min, mid>(b); }
template<class T> constexpr auto top_right(const Box<T, 3>& b) { return box_point<max, min, mid>(b); }
template<class T> constexpr auto left(const Box<T, 3>& b) { return box_point<min, mid, mid>(b); }
template<class T> constexpr auto center(const Box<T, 3>& b) { return box_point<mid, mid, mid>(b); }
template<class T> constexpr auto right(const Box<T, 3>& b) { return box_point<max, mid, mid>(b); }
template<class T> constexpr auto bottom_left(const Box<T, 3>& b) { return box_point<min, max, mid>(b); }
template<class T> constexpr auto bottom(const Box<T, 3>& b) { return box_point<mid, max, mid>(b); }
template<class T> constexpr auto bottom_right(const Box<T, 3>& b) { return box_point<max, max, mid>(b); }

template<class T> constexpr auto back_top_left(const Box<T, 3>& b) { return box_point<min, min, max>(b); }
template<class T> constexpr auto back_top(const Box<T, 3>& b) { return box_point<mid, min, max>(b); }
template<class T> constexpr auto back_top_right(const Box<T, 3>& b) { return box_point<max, min, max>(b); }
template<class T> constexpr auto back_left(const Box<T, 3>& b) { return box_point<min, mid, max>(b); }
template<class T> constexpr auto back(const Box<T, 3>& b) { return box_point<mid, mid, max>(b); }
template<class T> constexpr auto back_right(const Box<T, 3>& b) { return box_point<max, mid, max>(b); }
template<class T> constexpr auto back_bottom_left(const Box<T, 3>& b) { return box_point<min, max, max>(b); }
template<class T> constexpr auto back_bottom(const Box<T, 3>& b) { return box_point<mid, max, max>(b); }
template<class T> constexpr auto back_bottom_right(const Box<T, 3>& b) { return box_point<max, max, max>(b); }
```

Aliases for finding important box points (corners, center of edges, center of
faces, center of object).

### Intersection

```cpp
template<class T, class U, size_t N> constexpr bool contains(const Box<T, N>& box, const Vec<U, N>& vec);
template<class T, class U, size_t N> constexpr bool contains(const Box<T, N>& box, const Box<U, N>& box2);
```

Check if `box` contains the given object.

```cpp
template<class T, class U, size_t N> constexpr bool intersects(const Box<T, N>& box, const Box<U, N>& box2);
```

Check if the given boxes intersect.
