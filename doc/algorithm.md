
# Algorithms

Defined in [`geom/algorithm.h`](../geom/algorithm.h).

## Number Casting

Casts between `Num` types with the same units, but different representation and
ratios. Allows specifying if rounding will happen downwards or upwards.

For numbers and vectors, the `down_cast()` and `up_cast()` operations are
provided: the first rounds downwards and the second upwards.

For intervals and boxes, in addition to the former, the `in_cast()` and
`out_cast()` operations are provided: the first rounds towards the center of
the interval or box, and the second apart from it.

### Num

```cpp
template<num To, num From> To down_cast(const From& n);
template<num To, num From> To up_cast(const From& n);
```

### Vec

```cpp
template<num To, vec From> Vec<To, dimension_v<From>> down_cast(const From& n);
template<num To, vec From> Vec<To, dimension_v<From>> up_cast(const From& n);
```

### Interval

```cpp
template<num To, interval From> Interval<To> down_cast(const From& n);
template<num To, interval From> Interval<To> up_cast(const From& n);
template<num To, interval From> Interval<To> in_cast(const From& n);
template<num To, interval From> Interval<To> out_cast(const From& n);
```

### Box

```cpp
template<num To, box From> Box<To, dimension_v<From>> down_cast(const From& n);
template<num To, box From> Box<To, dimension_v<From>> up_cast(const From& n);
template<num To, box From> Box<To, dimension_v<From>> in_cast(const From& n);
template<num To, box From> Box<To, dimension_v<From>> out_cast(const From& n);
```

## Table Algorithms

### Resize

```cpp
template<table T> T resize(const T& table, const table_box_t<T>& box);
```

Constructs a new table of size `box.size()` that contains elements copied from
`table` at positions offset by `box.min()`.

### Grow

```cpp
template<table T> T grow(const T& table, const table_box_t<T>& box);
```

Constructs a new table from `table`, with extra cells in each side equal to the
value of the given box; note that the box defines how many extra cells will be
inserted instead of the size of the new table.

### Shrink

```cpp
template<table T> T shrink(const T& table, const table_box_t<T>& box);
```

Constructs a new table from `table`, removing cells in each side equal to the
value of the given box; note that the box defines how many extra cells will be
removed instead of the size of the new table.

### Shave

```cpp
template<table T, std::predicate<const typename T::value_type&> P>
table_box_t<T> shave(const T& table, P pred);
```

Returns the smallest box inside `table` such as it contains every cell
satisfying `pred`.
