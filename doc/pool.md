
# geom::Pool

Defined in [`geom/pool.h`](../geom/pool.h)

```cpp
template<class T, typename Enable = void> struct Pool;
template<class T> struct Pool<T, std::enable_if_t<std::is_trivial_v<T>>>;
```

`geom::Pool` provides a pool of elements stored as near each other as it can,
that are addressed by indices instead of pointers.

The generic version handles any type and stores elements in `std::deque`,
calling their constructors and destructors on insert() and erase() calls.

The version for trivial classes stores its objects in a simpler `std::vector`
and doesn't bother with constructors and destructors.

## Member Types

```cpp
using value_type = T;
using reference = T&;
using const_reference = const T&;
using pointer = T*;
using const_pointer = const T*;
using size_type = size_t;
```

## Member Functions

```cpp
size_type insert(const_reference value);
```

Insert new element in the pool and return its index.

```cpp
void erase(size_type pos);
```

Erase element with given index from the pool.

```cpp
reference operator[](size_type pos);
const_reference operator[](size_type pos) const;
```

Return a reference to the element with the given index.

