
# geom::Interval

Defined in [`geom/interval.h`](../geom/interval.h).

```cpp
template<class T> struct Interval;
```

`geom::Interval` is a class template that models a type-safe range of
[`geom::Num`](num.md) type.


## Member Types

```cpp
using value_type = Interval<T>;
using reference = value_type&;
using const_reference = const value_type&;
using pointer = value_type*;
using const_pointer = const value_type*;
```

## Member objects

```cpp
value_type min;
value_type max;
```

## Member Functions

### Constructors

```cpp
constexpr Interval();
constexpr Interval(T min, T max);
template<class U> constexpr Interval(const U& min, const U& max);
template<class U> constexpr Interval(const Interval<U>& other);
```

Constructs a new `Interval` from:

1. nothing, the values are uninitialized;
2. a minimum and maximum value that are copied;
3. a minimum and maximum value that are that are transformed;
4. Another Interval whose representation is transformed; if precision is lost, the
   number is rounded towards zero.

### Assignment

```cpp
template<class U> Interval& operator=(const Interval<U>&) = default;
```

Assigns the contents of one `Interval` to another.

### Unary Minus

```cpp
Interval operator-() const;
```

Implements unary minus.

### Compound Assignment

```cpp
template<class U> Interval& operator+=(const U& r);
template<class U> Interval& operator-=(const U& r);
template<Integral I> Interval& operator*=(I r);
template<Integral I> Interval& operator/=(I r);
template<Integral I> Interval& operator%=(I r);
```

Performs compound assignment between a `Interval` and a `Num` or between a `Interval` and
a plain number.

### Basic Arithmetic

Performs basic arithmetic operations between a `Interval` and a `Num`, or between a
`Interval` and a plain number.

```cpp
template<class T, class U>
constexpr auto operator+(const Interval<T>& l, const U& l)
template<class T, class U>
constexpr auto operator+(const T& l, const Interval<U>& l)
template<class T, class U>
constexpr auto operator-(const Interval<T>& l, const U& l);
```

Addition and substraction is performed between a `Interval` and a `Num` object; the
type of the returned object is a `Interval` whose basic type is the common type of
both the inputs.

```cpp
template<class T, Integral I>
constexpr auto operator*(const Interval<T>& l, I r);
template<class T, Integral I>
constexpr auto operator*(I l, const Interval<T>& r);
```

Multiplication is performed between a `Interval` object and a corresponding
`Interval::rep` value, in either order; the return value is always `Interval`.

```cpp
template<class T, Integral I>
constexpr auto operator/(const Interval<T>& l, I r);
template<class T, Integral I>
constexpr auto operator%(const Interval<T>& l, I r);
```

Both division and modulus can be performed between a `Interval` object and its
corresponding `Interval::rep` value, and return a new `Interval` object.

### Comparison

```cpp
template<class T, class U>
constexpr bool operator==(const Interval<T>& l, const Interval<U>& l);
template<class T, class U>
constexpr bool operator!=(const Interval<T>& l, const Interval<U>& l);
```

Compares two `Interval` with the same tag.

### Formatting 

```cpp
template<class T>
std::ostream& operator<<(std::ostream& s, const Interval<T>& v);
```

Inserts a textual representation of `v` into `s`.

```cpp
template<class T>
std::string to_string(const Interval<T>& v);
```

Converts a `Interval` to `std::string`.

