
# geom::Tree_leaves

Declared in [`geom/tree.h`](../geom/tree.h).

```cpp
template<class Handle, class T, class Enable = void> class Tree_leaves;
template<class Handle, class T> class Tree_leaves<Handle, T,
        std::enable_if_t<std::is_trivial_v<T> && sizeof(T) < sizeof(Handle)>>;
```

`geom::Tree_leaves` provides storage for the data held by a
[`geom::Tree`](tree.md).

Node handles are negative integers to differentiate them from the handles of
inner nodes in the actual tree.

In the default version values are stored along with a reference counter in a
[`geom::Pool`](pool.md) and the handles are the negated indices of the list.

If the stored values are trivial and their size is less than that of the
handle, the values are stored inside the handle instead.

## Member Types

```cpp
using node_handle = Handle;
using value_type = T;
using size_type = size_t;
```

## Member Functions

### Insertion

```cpp
Handle insert(const T& value);
```

Returns an index that can be used with `operator[]` to retrieve a copy of
`value`.

### Copy

```cpp
Handle clone(Handle pos);
```

Copies `pos`, ensuring `erase(pos)` can be called once more before the value is
discarded.

### Removal

```cpp
void erase(Handle pos);
```

Discards a value.

### Element Access

```cpp
const T& operator[](Handle pos) const;
```

Returns the value associated with a handle returned by `insert()` or `clone()`.
