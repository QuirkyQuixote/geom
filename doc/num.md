
# geom::Num

Defined in [`geom/num.h`](../geom/num.h).

```cpp
template<std::integral I, class U, class F> struct Num;
```

`geom::Num` is a class template that models a type-safe scalar value and
provides automatic conversion between values of the same type with different
representation and/or scales.

`U` represents units:

- addition, substraction and comparison can only be performed between numbers
  with the same U type.

- multiplication, division, and modulus can be performed between any types for
  which `U1() + U2()` (for multiplication) and `U1() - U2()` (for division and
  modulus) is defined, and the result type of such operations determines the U
  that the resulting number will have.

Geom provides the `Unit<Us...>` class to implement this; adimensional numbers
can be implemented by providing `Unit<>`, with no arguments.

`F` represents a multiplication factor and is implemented by default with
`std::ratio<>`; to see the factor resulting of each operation see the
documentation for that particular function.

## Member Types

```cpp
using rep = I;
using units = U;
using factor = F;
```

## Member Functions

### Constructors

```cpp
constexpr Num() = default;
constexpr Num(const Num&) = default;
constexpr Num(Num&&) = default;
constexpr explicit Num(I n);
template<std::integral I2, class F2> constexpr Num(const Num<I2, U, R2>& n);
```

Constructs a new `Num` from:

1. Nothing, the internal representation is left uninitialized.
2. Another `Num` with the same tag whose representation is copied.
3. Another `Num` with the same tag whose representation is moved.
4. A representation that is copied.
5. Another `Num` with the same tag whose representation is transformed; if
   precision is lost, the number is rounded towards zero.

### Assignment

```cpp
Num& operator=(const Num&) = default;
Num& operator=(Num&&) = default;
```

Assigns the contents of one `Num` to another.

### Cast to Integer

```cpp
template<std::integral I2> constexpr explicit operator I2() const;
```

Returns the internal representation of the `Num`, cast to `I2`.

### Unary Plus and Minus

```cpp
constexpr Num operator+() const;
constexpr Num operator-() const;
```

Implements unary plus and minus.

### Increment and Decrement

```cpp
constexpr Num& operator++();
constexpr Num& operator--();
constexpr Num operator++(int);
constexpr Num operator--(int);
```

Increments or decrements the internal representation of the `Num`.

### Compound Assignment

```cpp
template<std::integral I2, class F2> constexpr Num& operator+=(const Num<I2, U, F2>& r);
template<std::integral I2, class F2> constexpr Num& operator-=(const Num<I2, U, F2>& r);
```

Performs compound assignment with a `Num` with the same tag.

```cpp
template<std::integral I2> constexpr Num& operator*=(I2 r);
template<std::integral I2> constexpr Num& operator/=(I2 r);
template<std::integral I2> constexpr Num& operator%=(I2 r);
```

Performs compound assignment with a plain number.

```cpp
template<Integral I2, class U2, class F2 I>
requires<std::same_as<decltype(U() + U2()), U>
constexpr Num& operator*=(const Num<I2, U2, F2>& r);

template<Integral I2, class U2, class F2 I>
requires<std::same_as<decltype(U() - U2()), U>
constexpr Num& operator/=(const Num<I2, U2, F2>& r);

template<Integral I2, class U2, class F2 I>
requires<std::same_as<decltype(U() - U2()), U>
constexpr Num& operator%=(const Num<I2, U2, F2>& r);
```

Performs compound assignment with a `Num` whose units do not modify the ones of
the current tag.

### Arithmetic

```cpp
template<std::integer I2, class F2>
constexpr Num<decltype(I() + I2()), U, common_ratio_t<F, F2>>
operator+(const Num<I2, U, F2>& r) const;

template<std::integer I2, class F2>
constexpr Num<decltype(I() - I2()), U, common_ratio_t<F, F2>>
operator-(const Num<I2, U, F2>& r) const;
```

Performs addition or substraction between `Num` with the same units.

```cpp
template<std::integral I2> constexpr Num<decltype(I() + I2()), U, F> operator*(I2 r) const;
template<std::integral I2> constexpr Num<decltype(I() - I2()), U, F> operator/(I2 r) const;
template<std::integral I2> constexpr Num<decltype(I() - I2()), U, F> operator%(I2 r) const;
```

Performs multiplication, division or modulus between a `Num` and an integer.

```cpp
template<std::integral I2, class U2, class F2>
constexpr Num<decltype(I() + I2()), decltype(U() + U2()), std::ratio_multiply<F, F2>>
operator*(const Num<I2, U2, F2>& r) const;

template<std::integral I2, class U2, class F2>
constexpr Num<decltype(I() / I2()), decltype(U() - U2()), std::ratio_divide<F, F2>>
operator/(const Num<I2, U2, F2>& r) const;

template<std::integral I2, class U2, class F2>
constexpr Num<decltype(I() % I2()), decltype(U() - U2()), std::ratio_divide<F, F2>>
operator%(const Num<I2, U2, F2>& r) const;
```

Performs multiplication, division or modulus between two `Num`.

### Comparison

```cpp
template<std::integral I2, class F2>
constexpr auto operator<=>(const Num<I2, U, F2>& r) const;

template<std::integral I2, class F2>
constexpr bool operator==(const Num<I2, U, F2>& r) const;

template<std::integral I2, class F2>
constexpr bool operator!=(const Num<I2, U, F2>& r) const;

template<std::integral I2, class F2>
constexpr bool operator<=(const Num<I2, U, F2>& r) const;

template<std::integral I2, class F2>
constexpr bool operator<(const Num<I2, U, F2>& r) const;

template<std::integral I2, class F2>
constexpr bool operator>=(const Num<I2, U, F2>& r) const;

template<std::integral I2, class F2>
constexpr bool operator>(const Num<I2, U, F2>& r) const;
```

Compares two `Num` with the same units.

## Non-member Functions

```cpp
template<std::integral I, class U, class F> I get(const Num<I, U, F>& x);
```

Returns the internal representation of the `Num`:

